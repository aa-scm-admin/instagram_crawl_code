#!/usr/bin/env perl -wl

use strict;
use warnings;
use utf8;
use JSON;
use Data::Dumper;

my $last    = 150; # Returns 150 Max anyway
my $csrf    = "414f12d750834b7347b49cabeda4280e";
my $mid     = "UxV2vgAEAAFLJb0FBVXALihQXLUz";
my $post_id = "hRlhk7pFM7";
my $CURL    = "/usr/bin/curl";
my $GUNZIP  = "/bin/gunzip";
my $headers = "-H 'Accept: application/json, text/javascript, */*; q = 0.01' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q = 0.5' -H 'Cache-Control: no-cache' -H 'Connection: keep-alive' -H 'Content-Type: application/x-www-form-urlencoded; charset = UTF-8' -H 'Cookie: csrftoken = $csrf; mid = $mid; ccode = IN' -H 'Host: instagram.com' -H 'Pragma: no-cache' -H 'Referer: http://instagram.com/p/hSgCYnJFMP/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0' -H 'X-CSRFToken: $csrf' -H 'X-Instagram-AJAX: 1' -H 'X-Requested-With: XMLHttpRequest'";

my $url = "$CURL $headers 'http://instagram.com/query/' --data 'q=ig_shortcode($post_id)+%7B+id%2C+code%2C+owner+%7B+id%2C+username%2C+profile_pic_url%2C+followed_by_viewer%2C+requested_by_viewer+%7D%2Cis_video%2C+video_url%2C+date%2C+display_src%2C+location+%7B+name+%7D%2C+caption%2C+likes+%7B+count%2C+viewer_has_liked%2C+nodes+%7B+user+%7B+username%2C+profile_pic_url%2C+followed_by_viewer%2C+requested_by_viewer+%7D+%7D+%7D%2C+comments.last($last)+%7B+nodes+%7B+id%2C+user+%7B+username%2C+profile_pic_url+%7D%2C+text%2C+viewer_can_delete+%7D%7D%7D'";

chomp(my $out = `$url | $GUNZIP`);

print $out;

my $json = decode_json($out);

print Dumper($json);
