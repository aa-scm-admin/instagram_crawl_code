package InstagramRelationships;

use strict;
use warnings;
use utf8;
use DBI;
use Data::Dumper;
use InstagramLog;
use Sys::Hostname;
use InstagramConstants;

sub new {
    my $class  = shift;
    my $config = shift;
    my $dbh    = shift;
    my $self  = {
        config => $config,
        rels   => {},
        _dbh   => $dbh
    };
    bless($self, $class);    # make $self an object of class $class
    # Fetch all the relationships from the database
    my $tbl = $self->getTableName("relationship_map");
    my $sql = "SELECT id, rel from $tbl";
    my $sth = $self->{_dbh}->prepare($sql);
    my $rc  = 0;
    $sth->execute || die $!;
    while (my $row = $sth->fetchrow_hashref) {
        $self->{rels}->{forward}->{$row->{rel}} = $row->{id};
        $self->{rels}->{reverse}->{'r'.$row->{id}}  = $row->{rel};
        $rc++;
    }
    my $xi = $self->{config}->{death}->{when}->{min_relationship_maps};
    if (not defined $xi or $rc < $xi) {
        &w_error("Aborting as there are not enough (required = $xi, found = $rc) relationships found in $tbl table");
        exit 1;
    }
    return $self;
}

sub rel_fw {
    my ($self, $name) = @_;
    return $self->{rels}->{forward}->{$name} if defined $self->{rels}->{forward}->{$name};
    &w_error("Programming error. $name is not a valid relationship as per database. Please correct it before continuing.");
    exit 1;
}
sub rel_rr {
    my ($self, $num) = @_;
    return $self->{rels}->{reverse}->{'r'.$num} if defined $self->{rels}->{forward}->{'r'.$num};
    &w_error("Programming error. $num is not a valid relationship-value as per database. Please correct it before continuing.");
    exit 1;
}

sub getTableName($) {
    my $self  = shift;
    my $name  = shift;
    my $table = $self->{config}->{tables}->{$name};
    if (not defined $table) {
        &w_exit("'$name' is not found in Instagram.json. Aborting");
    }
    return $table;
}

1;
