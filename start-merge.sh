#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi

DIR="./$1/"

echo "Checking profiles which are still in crawling state ...";
mysql -uinstagram -hstage5 -A instagram_crawl -pinstagrampass -t -e"select * from job_entities where status IN ('new', 'crawling')";

echo "Putting any left-over crawls to crawled state ...";
mysql -uinstagram -hstage5 -A instagram_crawl -pinstagrampass -t -e"update job_entities set status = 'crawled' where status IN ('new', 'crawling')";

echo "Starting the merge ...";
mkdir -p $DIR && {
    for CAT in `mysql -uinstagram -hstage5 -A instagram -pinstagrampass -e'show tables like "%master%"' | grep master | cut -d_ -f3 | grep -v ^insta | grep -v -e ALL -e bk`
    do
        echo "Starting $CAT"
        rm -f $CAT.stopper && rm -f stopper.$CAT
	echo $1
        #perl instagram-merge.pl $CAT >> "$DIR/$CAT.log" 2>&1 &
        perl instagram-merge.pl --category $CAT --mergeday $1 >> "$DIR/$CAT.log" 2>&1 &
        echo -e "\t Log file is $DIR/$CAT.log"
    done
    disown -a
}
