#!/bin/bash

for CAT in `mysql -uinstagram -hstage5 -A instagram -pinstagrampass -e'show tables like "%master%"' | grep master | cut -d_ -f3 | grep -v ^insta`
do
    table=instagram.entity_fan_${CAT}_master
    echo "optimizing $table"
    echo "optimize table $table" | mysql -t -uinstagram -hstage5 -pinstagrampass -A
done

for table in all_networks.UUID_ig instagram_universe.fan_universe
do
    echo "optimizing $table"
    echo "optimize table $table" | mysql -t -uinstagram -hstage5 -pinstagrampass -A
done
