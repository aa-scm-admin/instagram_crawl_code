#!/bin/bash

# NOTE: These queries take hell lot of time !!!!

# Insert all the commenters into UUID_ig
# NOTE: Ordering is important so that the timestamp is updated accordingly
echo "Inserting all the commenters into UUID_ig";
mysql -t -uinstagram -pinstagrampass -A -e"insert into all_networks.UUID_ig (fan_id, last_updt_time) (select userid, from_unixtime(created_time) from entity_fan where comment_id != 0 order by created_time asc) on duplicate key update last_updt_time = from_unixtime(created_time)" instagram_crawl

echo "Inserting all the likers into UUID_ig";
# Insert all the likers into UUID_ig (timestamp is the only difference)
# NOTE: Ordering is important so that the timestamp is updated accordingly
mysql -t -uinstagram -pinstagrampass -A -e"insert ignore into all_networks.UUID_ig (fan_id, last_updt_time) (select userid, from_unixtime(created_time) from entity_fan where comment_id = 0 order by created_time asc)" instagram_crawl

echo "Adjusting timestamps for commenters from entity_fan into UUID_ig";
mysql -t -uinstagram -pinstagrampass -A -e"update all_networks.UUID_ig A JOIN entity_fan B ON A.fan_id = B.userid SET A.last_updt_time = FROM_UNIXTIME(GREATEST(UNIX_TIMESTAMP(A.last_updt_time), B.created_time)) where B.comment_id != 0;" instagram_crawl

#echo "Adjusting timestamps for likers from entity_fan into UUID_ig";
#mysql -t -uinstagram -pinstagrampass -A -e"update all_networks.UUID_ig A JOIN entity_fan B ON A.fan_id = B.userid SET A.last_updt_time = B.from_unixtime(created_time) where B.comment_id != 0)"
