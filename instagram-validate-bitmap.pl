#!/usr/bin/env perl -wl

use strict;
use warnings;
use utf8;
use Data::Dumper;
use File::Slurp qw(read_file);
use JSON;
use InstagramBitmapVerify;
use Instagram;
use InstagramCrawlDB;
use InstagramLog;
use Cwd;
use FindBin;
use File::Spec;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Validates that no fans are lost as part of the merge process
# Date: Jun/2014

my $in_category     = shift;
my $debug           = defined $ENV{DEBUG} ? 1 : 0;
my $verbose         = defined $ENV{VERBOSE} ? 1 : 0;
my $cwd             = getcwd;
my $mypath          = $FindBin::Bin;
my $stopper_file    = "$mypath/stopper";

binmode(STDIN,  ':encoding(UTF-8)');
binmode(STDOUT, ':encoding(UTF-8)');
binmode(STDERR, ':encoding(UTF-8)');

my @queue    = ();
my $cfg_data = read_file("$mypath/Instagram.json");
my $config;
eval {
    $config = decode_json($cfg_data);
};
if ($@) {
    print $@;
    exit 1;
}

# Main program starts here
my $total_processed = 0;
if (-e $stopper_file) {
    # Exit early if stopper file is found.
    &w_info("Found stopper file $stopper_file. Exiting ....");
} else {
    # Continue normal processing
    my $idb = InstagramBitmapVerify->new($config);
    $idb->setup();
    while (my $profile = $idb->fetchProfileToValidate($in_category)) {
        &w_exit("No more profiles found.") if scalar(keys %$profile) < 3;
        $idb->processProfile($profile);
        $total_processed++;
        if (-e $stopper_file) {
            &w_info("Found stopper file $stopper_file. Exiting ....");
        }
    }
    &w_info("Reset successful on ".$idb->resetAll()." profiles");
}

&w_info("Processed $total_processed profiles from database.");

exit(0);
