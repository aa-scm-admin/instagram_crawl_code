#!/bin/bash


if [ -z $1 ];then
    echo "syntax: $0 <catalogweek>";
    exit
fi

catweek=$1

# Program do download the database files from remote EC2 nodes
echo "Started downloading data from remote EC2 server - $(date)"
for host in `awk '/db/ {print $3}' hosts.txt`
do
    echo Copying from $host
    #scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* .
    ssh -i ~/.ssh/affinityanswers.pem centos@gongura-compute1.affinityanswers.com "mkdir /home/centos/instagram/$catweek"

    scp -r -o 'StrictHostKeyChecking=no' -C -i ~/.ssh/affinityanswers.pem root@$host:/disk/xvdg/completed-backup* centos@gongura-compute1.affinityanswers.com:/home/centos/Instagram/$catweek
done

# Print details
echo "Completed. All backups are present under $(pwd)/completed-backups/ directory";
#ls -l $(pwd)/completed-backups/
    ssh -i ~/.ssh/affinityanswers.pem centos@gongura-compute1.affinityanswers.com "ls -l /home/centos/instagram/$catweek"

# print status
echo "All completed - $(date)."
