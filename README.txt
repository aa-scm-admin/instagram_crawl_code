Steps to run INSTAGRAM crawl:

1. Login to loader3.colligent.com
	command: ssh loader3.colligent.com

2. Create a seperate screen to run the crawl.
Command used: screen -S ‘//a name for screen’
To detach from screen can use command “ctrl + A + D”  
“ctrl + A + C” - create new screen inside one screen 
To know the list of screens which are running: screen -ls
For again using an existing screen: screen -dra 32109.InstCrawl
32109.InstCrawl – the screen which is running

3. login as ‘mobydick’ user. Command: sudo su – mobydick

4. Use directory Instagram:
<mobydick || @loader3 ~>$ cd Instagram

5. Create a new directory giving name as next catalog week date:
<mobydick || @loader3 Instagram>$ mkdir -p auto-provision/2016dec28

6. For rerouting to the new catalog week just created give the following command:
<mobydick || @loader3 Instagram>$ cd $_
<mobydick || @loader3 2016dec28>$

7. Create a new instance in Amazon ec2 using the shell script:
<mobydick || @loader3 2016dec28>$ bash ../../launch-ec2-instance.sh create
1. Provisioning new server 'db' from 'ami-b3bf2f83' : [success] (db.create.log)
2. Waiting for Public IP allocation for i-0b3b7bcfe38df8059 and 172.31.15.176 : [success] (public-ip = 35.166.4.46, log = db.public_ip_wait.log)
3. Assigning hostname db : [success] (name = db, log = db.tag.log)

1. Provisioning new server 'crawl1' from 'ami-c7d092f7' : [success] (crawl1.create.log)
2. Waiting for Public IP allocation for i-070aafeba74ff10b7 and 172.31.17.65 : [success] (public-ip = 35.163.212.47, log = crawl1.public_ip_wait.log)
3. Assigning hostname : [success] (name = crawl1, log = crawl1.tag.log)
4. Setting up firewall rules :  [3306 : success] [6379 : success]
1. Provisioning new server 'crawl2' from 'ami-c7d092f7' : [success] (crawl2.create.log)
2. Waiting for Public IP allocation for i-0d35dda8094045be7 and 172.31.18.244 : [success] (public-ip = 35.160.133.73, log = crawl2.public_ip_wait.log)
3. Assigning hostname : [success] (name = crawl2, log = crawl2.tag.log)
4. Setting up firewall rules :  [3306 : success] [6379 : success]
1. Provisioning new server 'crawl3' from 'ami-c7d092f7' : [success] (crawl3.create.log)
2. Waiting for Public IP allocation for i-05c131e20fcea33eb and 172.31.22.98 : [success] (public-ip = 35.165.244.252, log = crawl3.public_ip_wait.log)
3. Assigning hostname : [success] (name = crawl3, log = crawl3.tag.log)
4. Setting up firewall rules :  [3306 : success] [6379 : success]
1. Provisioning new server 'crawl4' from 'ami-c7d092f7' : [success] (crawl4.create.log)
2. Waiting for Public IP allocation for i-0f6ecb2fbf53170ce and 172.31.21.68 : [success] (public-ip = 35.165.16.129, log = crawl4.public_ip_wait.log)
3. Assigning hostname : [success] (name = crawl4, log = crawl4.tag.log)
4. Setting up firewall rules :  [3306 : success] [6379 : success]
1. Provisioning new server 'crawl5' from 'ami-c7d092f7' : [success] (crawl5.create.log)
2. Waiting for Public IP allocation for i-0e2dafcf0bb79d049 and 172.31.28.134 : [success] (public-ip = 35.166.174.240, log = crawl5.public_ip_wait.log)
3. Assigning hostname : [success] (name = crawl5, log = crawl5.tag.log)
4. Setting up firewall rules :  [3306 : success] [6379 : success]


crawl1 172.31.17.65 35.163.212.47
crawl2 172.31.18.244 35.160.133.73
crawl3 172.31.22.98 35.165.244.252
crawl4 172.31.21.68 35.165.16.129
crawl5 172.31.28.134 35.166.174.240
db 172.31.15.176 35.166.4.46
<mobydick || @loader3 2016dec28>$

8. Change the IP in hosts.txt (Preferrably do outside the screen, since cat and vi command wont be efficient in screens)
<mobydick || @loader3 Instagram>$ cat hosts.txt
#hostname private-ip

crawl1 172.31.44.152 35.163.248.77
crawl2 172.31.41.20 35.166.120.201
crawl3 172.31.46.203 35.166.83.35
crawl4 172.31.35.137 35.165.113.190
crawl5 172.31.46.182 35.166.51.37
db 172.31.4.10 35.162.178.226
<mobydick || @loader3 Instagram>$ vi hosts.txt
<mobydick || @loader3 Instagram>$ cat hosts.txt
#hostname private-ip

crawl1 172.31.17.65 35.163.212.47
crawl2 172.31.18.244 35.160.133.73
crawl3 172.31.22.98 35.165.244.252
crawl4 172.31.21.68 35.165.16.129
crawl5 172.31.28.134 35.166.174.240
db 172.31.15.176 35.166.4.46
<mobydick || @loader3 Instagram>$



9. Make sure, changes are made in loader3, Instagram.
Copy the weekly catalog’s Entity_Catalog.txt to the loader3
<mobydick || @loader3 Instagram>$ cp /ccdata/catalog/2016dec28/Entity_Catalog.txt Entity_Catalog.txt

10. git commit hosts.txt and Entity_Catalog.txt
<mobydick || @loader3 Instagram>$ git commit hosts.txt Entity_Catalog.txt -m '2016dec28'
[2014jul30 805c108] 2016dec28
 2 files changed, 232 insertions(+), 161 deletions(-)
<mobydick || @loader3 Instagram>$

11. Host content have to changed in backend1 also. Login to backend1 as centos user and make changes in hosts.txt
[centos@backend1 ~]$ cd /mnt/gonguradrive/instagram
[centos@backend1 instagram]$ cat hosts.txt
#hostname private-ip

crawl1 172.31.44.152 35.163.248.77
crawl2 172.31.41.20 35.166.120.201
crawl3 172.31.46.203 35.166.83.35
crawl4 172.31.35.137 35.165.113.190
crawl5 172.31.46.182 35.166.51.37
db 172.31.4.10 35.162.178.226
[centos@backend1 instagram]$ vi hosts.txt
[centos@backend1 instagram]$ cat hosts.txt
#hostname private-ip

crawl1 172.31.17.65 35.163.212.47
crawl2 172.31.18.244 35.160.133.73
crawl3 172.31.22.98 35.165.244.252
crawl4 172.31.21.68 35.165.16.129
crawl5 172.31.28.134 35.166.174.240
db 172.31.15.176 35.166.4.46
[centos@backend1 instagram]$

12. Then from mobydick, run the setup file for crawls. (will take nearly an hour)
<mobydick || @loader3 Instagram>$ ./remote-crawl-setup.sh 2016dec28
Connecting as : centos
Setting up new node with ip 35.163.212.47
Connecting as : centos
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : centos
----> setting up for crawl node (logfile is setup.log)
[complete]
Connection to 35.163.212.47 closed.
Connecting as : centos
Setting up new node with ip 35.160.133.73
Connecting as : centos
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : centos
----> setting up for crawl node (logfile is setup.log)
[complete]
Connection to 35.160.133.73 closed.
Connecting as : centos
Setting up new node with ip 35.165.244.252
Connecting as : centos
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : centos
----> setting up for crawl node (logfile is setup.log)
[complete]
Connection to 35.165.244.252 closed.
Connecting as : centos
Setting up new node with ip 35.165.16.129
Connecting as : centos
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : centos
----> setting up for crawl node (logfile is setup.log)
[complete]
Connection to 35.165.16.129 closed.
Connecting as : centos
Setting up new node with ip 35.166.174.240
Connecting as : centos
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : centos
----> setting up for crawl node (logfile is setup.log)
[complete]
Connection to 35.166.174.240 closed.
<mobydick || @loader3 Instagram>$

13. run the setup file for database.
<mobydick || @loader3 Instagram>$ ./remote-db-setup.sh 2016dec28
Connecting as : root
Setting up new node with ip 35.166.4.46
Connecting as : root
Copying the source code .... : [success]
tcgetattr: Invalid argument
Connecting as : root
-----> Setting up for db node (logfile is setup.log) : [completed]
== all done. rebooting in 10 secs==

Broadcast message from root@db
        (/dev/pts/0) at 19:10 ...

The system is going down for reboot NOW!
Connection to 35.166.4.46 closed.
Connection closed by 35.166.4.46
<mobydick || @loader3 Instagram>$

14. Check the crawl nodes:
<mobydick || @loader3 Instagram>$ ./login-to-crawl-nodes.sh
Logging in to 35.164.67.95
Last login: Sun Jan  8 20:15:42 2017 from 4a.ae.1632.ip4.static.sl-reverse.com
[centos@crawl1 ~]$ 

15. Initially it will be as crawl1. In setup.log check for ‘htop’ presence, which will make sure that everything is installed correctly. Do the same in every crawl. Once check for first crawl is done, when we exit from that it will automatically login to next crawl. Since ./login-to-crawl-nodes.sh is written in such a way.

[centos@crawl1 ~]$ less setup.log
[centos@crawl1 ~]$ exit
logout
Connection to 35.164.67.95 closed.
Logging in to 35.166.33.136
Last login: Sun Jan  8 20:22:16 2017 from 4a.ae.1632.ip4.static.sl-reverse.com
[centos@crawl2 ~]$ less setup.log
[centos@crawl2 ~]$ 
[centos@crawl2 ~]$ exit
logout
Connection to 35.166.33.136 closed.
Logging in to 35.166.245.100
Last login: Sun Jan  8 20:28:14 2017 from 4a.ae.1632.ip4.static.sl-reverse.com
[centos@crawl3 ~]$ less setup.log 
[centos@crawl3 ~]$ exit
logout
Connection to 35.166.245.100 closed.
Logging in to 35.165.129.175
Last login: Sun Jan  8 20:32:52 2017 from 4a.ae.1632.ip4.static.sl-reverse.com
[centos@crawl4 ~]$ less setup.log 
[centos@crawl4 ~]$ 
[centos@crawl4 ~]$ exit
logout
Connection to 35.165.129.175 closed.
Logging in to 35.166.64.200
Last login: Sun Jan  8 20:38:44 2017 from 4a.ae.1632.ip4.static.sl-reverse.com
[centos@crawl5 ~]$ less setup.log 
[centos@crawl5 ~]$ 
[centos@crawl5 ~]$ exit
logout
Connection to 35.166.64.200 closed.

16. Then from mobydick, login to db nodes using the following shell script:
<mobydick || @loader3 Instagram>$ ./login-to-db-nodes.sh 
Logging in to 35.166.4.46
Last login: Mon Dec 26 19:05:59 2016 from 50.22.174.74
[root@ip-172-31-15-176 ~]# 

17. Login to mysql and use  “instagram_crawl” database.
[root@ip-172-31-15-176 ~]# mysql -uroot
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.5.54 MySQL Community Server (GPL) by Remi

Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

root@localhost [(none)]> use instagram_crawl;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed

18. Check the status from job_entities table and follow the following commands.
root@localhost [instagram_crawl]> select status,crawl_code,count(*) from job_entities group by status,crawl_code;
+--------+------------+----------+
| status | crawl_code | count(*) |
+--------+------------+----------+
| new    | NULL       |    23083 |
+--------+------------+----------+
1 row in set (0.04 sec)

root@localhost [instagram_crawl]> select count(max_post_age),max_post_age from job_entities group by max_post_age;
+---------------------+--------------+
| count(max_post_age) | max_post_age |
+---------------------+--------------+
|                 130 |            0 |
|               22953 |      2592000 |
+---------------------+--------------+
2 rows in set (0.01 sec)

root@localhost [instagram_crawl]> update job_entities set max_post_age = 180*86400 where max_post_age = 2592000;
Query OK, 22953 rows affected (0.13 sec)
Rows matched: 22953  Changed: 22953  Warnings: 0

root@localhost [instagram_crawl]> select count(max_post_age),max_post_age from job_entities group by max_post_age;
+---------------------+--------------+
| count(max_post_age) | max_post_age |
+---------------------+--------------+
|                 130 |            0 |
|               22953 |     15552000 |
+---------------------+--------------+
2 rows in set (0.01 sec)

root@localhost [instagram_crawl]> select status,crawl_code,count(*) from job_entities group by status,crawl_code;
+--------+------------+----------+
| status | crawl_code | count(*) |
+--------+------------+----------+
| new    | NULL       |    23083 |
+--------+------------+----------+
1 row in set (0.01 sec)

root@localhost [instagram_crawl]> 

19. Once checks all done in db, login to scrawl nodes to start the crawl.
<mobydick || @loader3 Instagram>$ ./login-to-crawl-nodes.sh 
Logging in to 35.163.212.47
Last login: Mon Dec 26 19:31:59 2016 from 4a.ae.1632.ip4.static.sl-rever
[centos@crawl1 ~]$ 

20. In crawl1 start the crawl as follows:
[centos@crawl1 ~]$ ./start-crawl.sh 
[Mon Dec 26 19:35:13 PST 2016]     : Settings .....
NUM_JOBS     : 
CRAWL_DIR    : /home/centos
[Mon Dec 26 19:35:13 PST 2016]     : Settings .....
[Mon Dec 26 19:35:13 PST 2016] Number of crawl jobs : 2
[Mon Dec 26 19:35:13 PST 2016] Starting Job 1
[Mon Dec 26 19:35:13 PST 2016] Starting Job 2
[Mon Dec 26 19:35:13 PST 2016] All the jobs are disowned now ....
[Mon Dec 26 19:35:13 PST 2016] All jobs are complete. Please verify below output ...
centos    3397  3391  0 19:35 pts/0    00:00:00 perl /home/centos/instagram-crawler
centos    3399  3391  0 19:35 pts/0    00:00:00 perl /home/centos/instagram-crawler
... Good bye !
[centos@crawl1 ~]$ 

Instagram Merge and UUID Generation Process:
root@localhost [instagram_crawl]> select status,crawl_code,count(*) from job_entities group by status,crawl_code;
+---------+---------------+----------+
| status  | crawl_code    | count(*) |
+---------+---------------+----------+
| crawled | 200 OK        |    22950 |
| crawled | 404 Not Found |       79 |
| crawled | 404 OK        |        1 |
+---------+---------------+----------+
3 rows in set (0.00 sec)

root@localhost [instagram_crawl]> select count(*) from entity_fan;
+----------+
| count(*) |
+----------+
|  8584189 |
+----------+
1 row in set (0.00 sec)

root@localhost [instagram_crawl]> select aa_id as 'S.No', category, profile_id, url, crawl_code, crawl_hostname from job_entities where crawl_code like '%404%';
+-------+----------+------------------------+--------------------------------------------------+---------------+----------------+
| S.No  | category | profile_id             | url                                              | crawl_code    | crawl_hostname |
+-------+----------+------------------------+--------------------------------------------------+---------------+----------------+
|  2774 | ART      | the_real_layzie_bone   | http://instagram.com/the_real_layzie_bone        | 404 Not Found | crawl4         |
|  5807 | BRN      | DonJulioOficial        | http://instagram.com/DonJulioOficial             | 404 Not Found | crawl1         |
|  6759 | BRN      | scion                  | http://instagram.com/scion                       | 404 Not Found | crawl2         |
|  8099 | DIG      | bestreet               | http://instagram.com/bestreet                    | 404 Not Found | crawl2         |
|  8245 | DIG      | guitarplayermag        | http://instagram.com/guitarplayermag             | 404 Not Found | crawl5         |
| 12576 | TVS      | nbctv                  | http://instagram.com/nbctv                       | 404 Not Found | crawl2         |
| 19795 | BRN      | spirit.airlines        | https://instagram.com/spirit.airlines            | 404 Not Found | crawl5         |
| 30985 | TVS      | nova_pbs               | https://www.instagram.com/nova_pbs               | 404 Not Found | crawl4         |
| 32049 | DIG      | fanlala                | http://instagram.com/fanlala                     | 404 Not Found | crawl5         |
| 33169 | BRN      | adidassporteyewear     | https://www.instagram.com/adidassporteyewear     | 404 Not Found | crawl3         |
| 35413 | BRN      | eaglecreekgear         | https://www.instagram.com/eaglecreekgear         | 404 Not Found | crawl2         |
| 37515 | DIG      | huffpostlive           | http://instagram.com/huffpostlive                | 404 Not Found | crawl3         |
| 40518 | TVS      | wwe__network_          | https://www.instagram.com/wwe__network_          | 404 Not Found | crawl2         |
| 40674 | BRN      | fourloko               | https://instagram.com/fourloko                   | 404 Not Found | crawl4         |
| 40815 | CEL      | cristela9              | http://instagram.com/cristela9                   | 404 Not Found | crawl5         |
| 42501 | BRN      | patron                 | http://instagram.com/patron                      | 404 Not Found | crawl2         |
| 42672 | BRN      | MikesHardLemonade      | http://instagram.com/MikesHardLemonade           | 404 Not Found | crawl2         |
| 43203 | BRN      | PeroniUSA              | http://instagram.com/PeroniUSA                   | 404 Not Found | crawl5         |
| 43205 | BRN      | MichelobULTRA          | http://instagram.com/MichelobULTRA               | 404 Not Found | crawl2         |
| 43208 | BRN      | newcastle_brown_ale    | http://instagram.com/newcastle_brown_ale         | 404 Not Found | crawl2         |
| 43212 | BRN      | Leinenkugels           | http://instagram.com/Leinenkugels                | 404 OK        | crawl5         |
| 43214 | BRN      | ReddsAppleAle          | http://instagram.com/ReddsAppleAle               | 404 Not Found | crawl5         |
| 43284 | BRN      | cruzcampo              | https://instagram.com/cruzcampo                  | 404 Not Found | crawl5         |
| 44662 | BRN      | baileysofficial        | http://instagram.com/baileysofficial             | 404 Not Found | crawl4         |
| 44748 | BRN      | chandonusa             | https://instagram.com/chandonusa                 | 404 Not Found | crawl5         |
| 45301 | DIG      | aoloriginals           | http://instagram.com/aoloriginals                | 404 Not Found | crawl4         |
| 45786 | BRN      | mydogsvoyce            | https://instagram.com/mydogsvoyce                | 404 Not Found | crawl4         |
| 48032 | BRN      | feel.good.foods        | https://instagram.com/feel.good.foods            | 404 Not Found | crawl5         |
| 48576 | BRN      | GalloFamily            | http://instagram.com/GalloFamily                 | 404 Not Found | crawl4         |
| 48757 | TVS      | fusionoutpost          | http://instagram.com/fusionoutpost               | 404 Not Found | crawl5         |
| 49661 | BRN      | olmecatequila          | http://instagram.com/olmecatequila               | 404 Not Found | crawl3         |
| 49766 | BRN      | swbasicsofbk           | http://instagram.com/swbasicsofbk                | 404 Not Found | crawl5         |
| 49783 | BRN      | paquitortillas         | http://instagram.com/paquitortillas              | 404 Not Found | crawl2         |
| 49969 | BRN      | cervejariabohemia      | https://instagram.com/cervejariabohemia          | 404 Not Found | crawl4         |
| 50513 | DIG      | The_Real_Outfit        | https://www.instagram.com/The_Real_Outfit        | 404 Not Found | crawl2         |
| 50672 | BRN      | newamsterdamspirits    | https://www.instagram.com/newamsterdamspirits    | 404 Not Found | crawl2         |
| 51138 | BRN      | finlandia_vodka        | http://instagram.com/finlandia_vodka             | 404 Not Found | crawl5         |
| 51141 | BRN      | tullamoredew           | http://instagram.com/tullamoredew                | 404 Not Found | crawl2         |
| 51875 | BRN      | tuiflycom              | https://instagram.com/tuiflycom                  | 404 Not Found | crawl2         |
| 53474 | SPR      | rio2016                | https://www.instagram.com/rio2016                | 404 Not Found | crawl2         |
| 53549 | BRN      | baunilhapijamasoficial | https://www.instagram.com/baunilhapijamasoficial | 404 Not Found | crawl5         |
| 53893 | BRN      | salad_br               | https://www.instagram.com/salad_br               | 404 Not Found | crawl5         |
| 57009 | DIG      | thewesttravel          | https://www.instagram.com/thewesttravel          | 404 Not Found | crawl4         |
| 57133 | LOC      | Heart1073              | https://www.instagram.com/Heart1073              | 404 Not Found | crawl2         |
| 57668 | BRN      | twlnzgaming            | https://www.instagram.com/twlnzgaming            | 404 Not Found | crawl5         |
| 59706 | CEL      | bell.josh19            | http://instagram.com/bell.josh19                 | 404 Not Found | crawl5         |
| 59966 | BRN      | chivasregal            | http://instagram.com/chivasregal                 | 404 Not Found | crawl3         |
| 60419 | BRN      | cervezaquilmes         | https://www.instagram.com/cervezaquilmes         | 404 Not Found | crawl2         |
| 60420 | BRN      | cervezabrahma          | https://www.instagram.com/cervezabrahma          | 404 Not Found | crawl4         |
| 60422 | BRN      | heineken_ar            | https://www.instagram.com/heineken_ar            | 404 Not Found | crawl5         |
| 60423 | BRN      | corona_argentina       | https://www.instagram.com/corona_argentina       | 404 Not Found | crawl2         |
| 60428 | BRN      | cervezapatagonia       | https://www.instagram.com/cervezapatagonia       | 404 Not Found | crawl2         |
| 60431 | BRN      | warsteinerargentina    | https://www.instagram.com/warsteinerargentina    | 404 Not Found | crawl2         |
| 60436 | BRN      | MixxTail_arg           | https://www.instagram.com/MixxTail_arg           | 404 Not Found | crawl5         |
| 60446 | BRN      | havanaclubar           | https://www.instagram.com/havanaclubar           | 404 Not Found | crawl3         |
| 60447 | BRN      | flordecanarum          | https://www.instagram.com/flordecanarum          | 404 Not Found | crawl3         |
| 60450 | BRN      | absolut_ar             | https://www.instagram.com/absolut_ar             | 404 Not Found | crawl3         |
| 60451 | BRN      | chivasregal_ar         | https://www.instagram.com/chivasregal_ar         | 404 Not Found | crawl3         |
| 60470 | BRN      | gancialive             | https://www.instagram.com/gancialive             | 404 Not Found | crawl3         |
| 61569 | BRN      | johnniewalker          | http://instagram.com/johnniewalker               | 404 Not Found | crawl5         |
| 62041 | BRN      | budlightliving         | https://www.instagram.com/budlightliving         | 404 Not Found | crawl2         |
| 62048 | BRN      | coorslightcanada       | https://www.instagram.com/coorslightcanada       | 404 Not Found | crawl3         |
| 62049 | BRN      | molsoncanadian         | https://www.instagram.com/molsoncanadian         | 404 Not Found | crawl2         |
| 62061 | BRN      | havanaclubca           | https://www.instagram.com/havanaclubca           | 404 Not Found | crawl4         |
| 62064 | BRN      | absolutcanada          | https://www.instagram.com/absolutcanada          | 404 Not Found | crawl3         |
| 62066 | BRN      | jpwisersca             | https://www.instagram.com/jpwisersca             | 404 Not Found | crawl4         |
| 62068 | BRN      | sonomacutrer           | https://www.instagram.com/sonomacutrer           | 404 Not Found | crawl5         |
| 62070 | BRN      | darkhorsewine          | https://www.instagram.com/darkhorsewine          | 404 Not Found | crawl4         |
| 62071 | BRN      | jacobscreekca          | https://www.instagram.com/jacobscreekca          | 404 Not Found | crawl5         |
| 63388 | BRN      | tanquerayusa           | https://www.instagram.com/tanquerayusa           | 404 Not Found | crawl4         |
| 63391 | BRN      | camarenatequila        | https://www.instagram.com/camarenatequila        | 404 Not Found | crawl2         |
| 63392 | BRN      | hornitostequila        | https://www.instagram.com/hornitostequila        | 404 Not Found | crawl2         |
| 63393 | BRN      | glenmorangiecom        | https://www.instagram.com/glenmorangiecom        | 404 Not Found | crawl2         |
| 63394 | BRN      | glenfiddichwhisky      | https://www.instagram.com/glenfiddichwhisky      | 404 Not Found | crawl3         |
| 63400 | BRN      | woodfordreserve        | https://www.instagram.com/woodfordreserve        | 404 Not Found | crawl4         |
| 63401 | BRN      | knobcreek              | https://www.instagram.com/knobcreek              | 404 Not Found | crawl4         |
| 63673 | BRN      | pacificobeer           | http://instagram.com/pacificobeer                | 404 Not Found | crawl5         |
| 63674 | BRN      | tocayobrewingco        | http://instagram.com/tocayobrewingco             | 404 Not Found | crawl4         |
| 64120 | BRN      | europacorp_studio      | https://www.instagram.com/europacorp_studio      | 404 Not Found | crawl5         |
| 64665 | CEL      | chkmbengue             | https://www.instagram.com/chkmbengue             | 404 Not Found | crawl4         |
+-------+----------+------------------------+--------------------------------------------------+---------------+----------------+
80 rows in set (0.01 sec)

root@localhost [instagram_crawl]> select aa_id as 'S.No', category, profile_id, url, crawl_code from job_entities where status = 'crawled' and  num_http_reqs = 0 and num_posts = 0;
+-------+----------+--------------------+--------------------------------------------+------------+
| S.No  | category | profile_id         | url                                        | crawl_code |
+-------+----------+--------------------+--------------------------------------------+------------+
|  2283 | ART      | jasminevillegas    | https://www.instagram.com/jasminevillegas  | 200 OK     |
|  2488 | ART      | lorenzojova        | http://instagram.com/lorenzojova           | 200 OK     |
|  2859 | ART      | lisaloeb           | http://instagram.com/lisaloeb              | 200 OK     |
|  2940 | ART      | lupefiasco         | http://instagram.com/lupefiasco            | 200 OK     |
|  3624 | ART      | agentcbw7          | https://www.instagram.com/agentcbw7        | 200 OK     |
|  5050 | ART      | unearthofficial    | http://instagram.com/unearthofficial       | 200 OK     |
| 19151 | BRN      | firekeepers        | https://instagram.com/firekeepers          | 200 OK     |
| 23978 | ART      | joshualedet        | http://instagram.com/joshualedet           | 200 OK     |
| 26277 | DIG      | badlipreading      | http://instagram.com/badlipreading         | 200 OK     |
| 28268 | CEL      | arianfoster        | http://instagram.com/arianfoster           | 200 OK     |
| 29214 | CEL      | varis9             | https://www.instagram.com/varis9           | 200 OK     |
| 36451 | ART      | rylandr5           | https://instagram.com/rylandr5             | 200 OK     |
| 39047 | BRN      | guaranaantarctica  | https://instagram.com/guaranaantarctica    | 200 OK     |
| 42096 | ART      | ryanmcdmusic       | http://instagram.com/ryanmcdmusic          | 200 OK     |
| 42343 | BRN      | cliniqueuk         | http://www.instagram.com/cliniqueuk        | 200 OK     |
| 43163 | ART      | gfresh_b           | https://www.instagram.com/gfresh_b         | 200 OK     |
| 43652 | ART      | shyglizzy          | https://instagram.com/shyglizzy            | 200 OK     |
| 47680 | BRN      | monstercanada      | https://instagram.com/monstercanada        | 200 OK     |
| 51329 | BRN      | Goldwellkmsacademy | http://instagram.com/Goldwellkmsacademy    | 200 OK     |
| 57099 | DIG      | CLEOAustralia      | https://www.instagram.com/CLEOAustralia    | 200 OK     |
| 58794 | BRN      | americaneagleuk    | https://www.instagram.com/americaneagleuk  | 200 OK     |
| 61240 | SPR      | gimnasia_oficial   | https://www.instagram.com/gimnasia_oficial | 200 OK     |
| 63462 | BRN      | logicecig          | https://www.instagram.com/logicecig        | 200 OK     |
| 65260 | TVS      | ArkhamTODK         | https://www.instagram.com/ArkhamTODK       | 200 OK     |
+-------+----------+--------------------+--------------------------------------------+------------+
24 rows in set (0.01 sec)

root@localhost [instagram_crawl]> select count(*) from entity_fan;
+----------+
| count(*) |
+----------+
|  8584189 |
+----------+
1 row in set (0.00 sec)

root@localhost [instagram_crawl]> select status,crawl_code,count(*) from job_entities group by status,crawl_code;
+---------+---------------+----------+
| status  | crawl_code    | count(*) |
+---------+---------------+----------+
| crawled | 200 OK        |    22950 |
| crawled | 404 Not Found |       79 |
| crawled | 404 OK        |        1 |
+---------+---------------+----------+
3 rows in set (0.00 sec)

root@localhost [instagram_crawl]>

<mobydick || @loader3 Instagram>$ pwd
/home/mobydick/Instagram
<mobydick || @loader3 Instagram>$ 
<mobydick || @loader3 Instagram>$ ./login-to-db-nodes.sh
Logging in to 35.162.178.226
Last login: Wed Dec 21 19:07:30 2016 from 50.22.174.74
[root@ip-172-31-4-10 ~]# 
[root@ip-172-31-4-10 ~]# ./backup-database-from-ec2-server.sh
ln: creating symbolic link `./completed-backups': File exists
Taking backup of job_entities
total 720
-rw-r--r-- 1 mysql mysql   2256 Dec 21 19:06 instagram_crawl.entity_fan.sql
-rw-r--r-- 1 mysql mysql   2810 Dec 21 19:10 instagram_crawl.job_entities.sql
-rw-rw-rw- 1 mysql mysql 728132 Dec 21 19:10 job_entities.tsv.bz2
Taking backup of entity_fan
total 458600
-rw-rw-rw- 1 mysql mysql 468866491 Dec 21 19:11 entity_fan.tsv.bz2
-rw-r--r-- 1 mysql mysql      2256 Dec 21 19:10 instagram_crawl.entity_fan.sql
-rw-r--r-- 1 mysql mysql      2810 Dec 21 19:10 instagram_crawl.job_entities.sql
-rw-rw-rw- 1 mysql mysql    728132 Dec 21 19:10 job_entities.tsv.bz2
Taking backup of fan_universe
Taking backup of fan_universe into /disk/xvdg/completed-backups/fan_universe.tsv
Taking backup of post
total 641460
-rw-rw-rw- 1 mysql mysql 468866491 Dec 21 19:11 entity_fan.tsv.bz2
-rw-rw-rw- 1 mysql mysql 163988011 Dec 21 19:13 fan_universe.tsv.bz2
-rw-r--r-- 1 mysql mysql      2256 Dec 21 19:10 instagram_crawl.entity_fan.sql
-rw-r--r-- 1 mysql mysql      2810 Dec 21 19:10 instagram_crawl.job_entities.sql
-rw-r--r-- 1 root  root       2110 Dec 21 19:14 instagram_universe.post.sql
-rw-rw-rw- 1 mysql mysql    728132 Dec 21 19:10 job_entities.tsv.bz2
-rw-rw-rw- 1 mysql mysql  23249352 Dec 21 19:14 post.tsv.bz2
/disk/xvdg/completed-backups
total 641460
-rw-rw-rw- 1 mysql mysql 468866491 Dec 21 19:11 entity_fan.tsv.bz2
-rw-rw-rw- 1 mysql mysql 163988011 Dec 21 19:13 fan_universe.tsv.bz2
-rw-r--r-- 1 mysql mysql      2256 Dec 21 19:10 instagram_crawl.entity_fan.sql
-rw-r--r-- 1 mysql mysql      2810 Dec 21 19:10 instagram_crawl.job_entities.sql
-rw-r--r-- 1 root  root       2110 Dec 21 19:14 instagram_universe.post.sql
-rw-rw-rw- 1 mysql mysql    728132 Dec 21 19:10 job_entities.tsv.bz2
-rw-rw-rw- 1 mysql mysql  23249352 Dec 21 19:14 post.tsv.bz2
All completed ...
[root@ip-172-31-4-10 ~]#

[colligent@loader3 ~]$ ./bk_connect_as_coligent.sh
Last login: Wed Dec 21 05:27:16 2016 from 106.51.37.16
[colligent@backend1 ~]$ mysql -uroot -pindie -hlocalhost -A
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 333161
Server version: 5.5.47-MariaDB MariaDB Server

Copyright (c) 2000, 2015, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> use instagram_crawl
Database changed
MariaDB [instagram_crawl]> create table job_entities like job_entities_2016dec14;
Query OK, 0 rows affected (0.98 sec)

MariaDB [instagram_crawl]> create table entity_fan like entity_fan_2016dec14;
Query OK, 0 rows affected (0.79 sec)

MariaDB [instagram_crawl]> create table instagram_universe.post like  instagram_universe.post_2016dec14;
Query OK, 0 rows affected (0.27 sec)

MariaDB [instagram_crawl]>

[colligent@loader3 ~]$ ./bk_connect.sh
Last login: Tue Dec 13 22:54:14 2016 from 106.206.137.91
[centos@backend1 ~]$ cd /mnt/gonguradrive/instagram
[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram
[centos@backend1 instagram]$ date
Tue Dec 13 22:54:37 PST 2016
[centos@backend1 instagram]$ sh -x copy-db-data-from-ec2-to-aws.sh 2016dec21
+ '[' -z 2016dec21 ']'
+ catweek=2016dec21
++ date
+ echo 'Started downloading data from remote EC2 server - Tue Dec 13 22:56:18 PST 2016'
Started downloading data from remote EC2 server - Tue Dec 13 22:56:18 PST 2016
++ awk '/db/ {print $3}' hosts.txt
+ for host in '`awk '\''/db/ {print $3}'\'' hosts.txt`'
+ echo Copying from 35.165.235.21
Copying from 35.165.235.21
+ mkdir /mnt/gonguradrive/instagram/2016dec21
+ scp -r -o StrictHostKeyChecking=no -C -i /home/centos/.ssh/affinityanswers.pem 'root@35.165.235.21:/disk/xvdg/completed-backup*' /mnt/gonguradrive/instagram/2016dec21
Warning: Permanently added '35.165.235.21' (RSA) to the list of known hosts.
instagram_crawl.job_entities.sql                                                                                    100% 2810     2.7KB/s   00:00    
job_entities.tsv.bz2                                                                                                100%  712KB 711.9KB/s   00:00    
instagram_crawl.entity_fan.sql                                                                                      100% 2256     2.2KB/s   00:00   
+ rm 2016dec21_crawl5
++ date
+ echo ' Completed the Downloading Crawl Data - Wed Dec 21 19:52:02 PST 2016 '
 Completed the Downloading Crawl Data - Wed Dec 21 19:52:02 PST 2016
++ date
+ echo 'All completed - Wed Dec 21 19:52:02 PST 2016.'
All completed - Wed Dec 21 19:52:02 PST 2016.
[centos@backend1 instagram]$

 [centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram  
[centos@backend1 instagram]$ date
Wed Dec 21 23:17:18 PST 2016 
[centos@backend1 instagram]$ sh -x restore-ec2-db-data-in-aws-mysql.sh /mnt/gonguradrive/instagram/2016dec21/completed-backups/
+ '[' -z /mnt/gonguradrive/instagram/2016dec21/completed-backups/ ']'
+ '[' '!' -d /mnt/gonguradrive/instagram/2016dec21/completed-backups/ ']'
+ dir=/mnt/gonguradrive/instagram/2016dec21/completed-backups/
+ echo '************** WARNING ************************'
************** WARNING ************************
+ echo 'Please make sure you have renamed the following'
Please make sure you have renamed the following
+ echo 'tables in instagram_crawl database on stage5 '
tables in instagram_crawl database on stage5
+ echo ''

+ echo '-> job_entities'
-> job_entities
+ echo '-> entity_fan'
-> entity_fan
+ echo ''

+ echo '************** WARNING ************************'
************** WARNING ************************
+ echo ''

+ echo -e 'Are you sure to continue with restore ? (yes/no) : \c'
Are you sure to continue with restore ? (yes/no) : + read yesno
yes
+ [[ yes == \y\e\s ]]
++ date
+ echo 'Started restore operation at Wed Dec 21 23:18:56 PST 2016..'
Started restore operation at Wed Dec 21 23:18:56 PST 2016..
+ echo ''

+ echo '1. Restoring job_entities ...'
1. Restoring job_entities ...
+ echo -e '1.1 Uncompressing .. : \c'
1.1 Uncompressing .. : + '[' -f /mnt/gonguradrive/instagram/2016dec21/completed-backups//job_entities.tsv ']'
+ bunzip2 /mnt/gonguradrive/instagram/2016dec21/completed-backups//job_entities.tsv.bz2
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo -e '1.2 Restoring ... : \c'
1.2 Restoring ... : + mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_crawl '-eload data local infile '\''/mnt/gonguradrive/instagram/2016dec21/completed-backups//job_entities.tsv'\'' into table instagram_crawl.job_entities character set utf8mb4;'
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo

+ echo

+ echo '2. Restoring entity_fan ...'
2. Restoring entity_fan ...  
+ echo -e '2.1 Uncompressing ... : \c'
2.1 Uncompressing ... : + '[' -f /mnt/gonguradrive/instagram/2016dec21/completed-backups//entity_fan.tsv ']'
+ bunzip2 /mnt/gonguradrive/instagram/2016dec21/completed-backups//entity_fan.tsv.bz2
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo -e '2.2 Restoring ... : \c'
2.2 Restoring ... : + mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_crawl '-eload data local infile '\''/mnt/gonguradrive/instagram/2016dec21/completed-backups//entity_fan.tsv'\'' into table instagram_crawl.entity_fan character set utf8mb4;'
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo

+ echo

+ echo '3. Restoring fan_universe ...'
3. Restoring fan_universe ...
+ echo -e '3.1 Uncompressing ... : \c'
3.1 Uncompressing ... : + '[' -f /mnt/gonguradrive/instagram/2016dec21/completed-backups//fan_universe.tsv ']'
+ bunzip2 /mnt/gonguradrive/instagram/2016dec21/completed-backups//fan_universe.tsv.bz2
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo -e '3.2 Restoring ... : \c'
3.2 Restoring ... : + mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_universe '-eload data local infile '\''/mnt/gonguradrive/instagram/2016dec21/completed-backups//fan_universe.tsv'\'' replace into table instagram_universe.fan_universe character set utf8mb4;'
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo

+ echo

+ echo '3. Restoring post ...'
3. Restoring post ...
+ echo -e '2.1 Uncompressing ... : \c'
2.1 Uncompressing ... : + '[' -f /mnt/gonguradrive/instagram/2016dec21/completed-backups//post.tsv ']'
+ bunzip2 /mnt/gonguradrive/instagram/2016dec21/completed-backups//post.tsv.bz2
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo -e '2.2 Restoring ... : \c'
2.2 Restoring ... : + mysql -uinstagram -pinstagrampass -A -hlocalhost instagram_universe '-eload data local infile '\''/mnt/gonguradrive/instagram/2016dec21/completed-backups//post.tsv'\'' into table instagram_universe.post character set utf8mb4;'
+ '[' 0 -eq 0 ']'
+ echo '[completed]'
[completed]
+ echo

+ echo

++ date
+ echo 'All complete. Thu Dec 22 01:19:09 PST 2016'
All complete. Thu Dec 22 01:19:09 PST 2016
[centos@backend1 instagram]$

[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram
[centos@backend1 instagram]$ sh -x verify-encrypted-data-integrity.sh
+ echo 'Checking entity_fan table for any corruption ...'
Checking entity_fan table for any corruption ...
+ mysql -t -uinstagram -pinstagrampass -A -hlocalhost instagram_crawl '-eset @aa_key = "63306C6C6967656E7421416666696E697479416E7377657273"; select count(*) from entity_fan where hex(aes_encrypt(userid, unhex(@aa_key))) is NULL or hex(aes_encrypt(username, unhex(@aa_key))) is NULL'
+----------+
| count(*) |
+----------+
|        0 |
+----------+
[centos@backend1 instagram]$

[centos@backend1 instagram]$ cd
[centos@backend1 ~]$
[centos@backend1 ~]$ pwd
/home/centos
[centos@backend1 ~]$
[centos@backend1 ~]$ python ig_sentiment.py entity_fan
[centos@backend1 ~]$

MariaDB [instagram_crawl]> select count(*) , status, crawl_code from job_entities group by crawl_code;
+----------+---------+---------------+
| count(*) | status  | crawl_code    |
+----------+---------+---------------+
|    22950 | crawled | 200 OK        |
|       79 | crawled | 404 Not Found |
|        1 | crawled | 404 OK        |
+----------+---------+---------------+
3 rows in set (0.04 sec)

MariaDB [instagram_crawl]> select count(*) from entity_fan where polarity is null AND fan_relationship like '%comment%';
+----------+
| count(*) |
+----------+
|        0 |
+----------+
1 row in set (14.78 sec)

MariaDB [instagram_crawl]>

[centos@backend1 ~]$ cd /mnt/gonguradrive/instagram
[centos@backend1 instagram]$ sh -x pre-export-script.sh
+ echo 'Checking profiles which are still in crawling state ...'
Checking profiles which are still in crawling state ...
+ mysql -uinstagram -hlocalhost -A instagram_crawl -pinstagrampass -t '-eselect * from job_entities where status IN ('\''new'\'', '\''crawling'\'')'
+ echo 'Putting any left-over crawls to crawled state ...'
Putting any left-over crawls to crawled state ...
+ mysql -uinstagram -hlocalhost -A instagram_crawl -pinstagrampass -t '-eupdate job_entities set status = '\''crawled'\'' where status IN ('\''new'\'', '\''crawling'\'')'
[centos@backend1 instagram]$

MariaDB [instagram_crawl]> select count(*) from job_entities;
+----------+
| count(*) |
+----------+
|    23030 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from job_entities_2016dec14;
+----------+
| count(*) |
+----------+
|    22967 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from entity_fan_2016dec14;
+----------+
| count(*) |
+----------+
|  8606260 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from entity_fan;
+----------+
| count(*) |
+----------+
|  8584189 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]>

MariaDB [instagram_crawl]> alter table entity_fan rename entity_fan_2016dec21;
Query OK, 0 rows affected (0.02 sec)

MariaDB [instagram_crawl]> alter table job_entities rename job_entities_2016dec21;
Query OK, 0 rows affected (0.02 sec)

MariaDB [instagram_crawl]> insert into entity_fan_meta set tablename = 'instagram_crawl.entity_fan_2016dec21', status = 'merged', in_new_warehouse = 'NO';
Query OK, 1 row affected (0.01 sec)

MariaDB [instagram_crawl]>

[colligent@loader3 ~]$ ./bk
Last login: Thu Dec 22 02:57:37 2016 from 106.51.37.16
[colligent@backend1 ~]$ ./GonguraArchitecture/connect.sh
psql (9.2.13, server 8.0.2)
WARNING: psql version 9.2, server version 8.0.
         Some psql features might not work.
SSL connection (cipher: ECDHE-RSA-AES256-GCM-SHA384, bits: 256)
Type "help" for help.

gongura=# 
gongura=# select count(*) from events_ig;
   count   
-----------
 842887344
(1 row)

gongura=# select count(*) from incremental_ig;
 count 
-------
  8146
(1 row)

gongura=# delete from incremental_ig;
DELETE 8146
gongura=# select count(*) from incremental_ig;
 count 
-------
     0
(1 row)

gongura=#

[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram
[centos@backend1 instagram]$ locale
LANG=en_US
LC_CTYPE="en_US"
LC_NUMERIC="en_US"
LC_TIME="en_US"
LC_COLLATE="en_US"
LC_MONETARY="en_US"
LC_MESSAGES="en_US"
LC_PAPER="en_US"
LC_NAME="en_US"
LC_ADDRESS="en_US"
LC_TELEPHONE="en_US"
LC_MEASUREMENT="en_US"
LC_IDENTIFICATION="en_US"
LC_ALL=
[centos@backend1 instagram]$ export LC_CTYPE="en_US.UTF-8"
[centos@backend1 instagram]$ locale
LANG=en_US
LC_CTYPE=en_US.UTF-8
LC_NUMERIC="en_US"
LC_TIME="en_US"
LC_COLLATE="en_US"
LC_MONETARY="en_US"
LC_MESSAGES="en_US"
LC_PAPER="en_US"
LC_NAME="en_US"
LC_ADDRESS="en_US"
LC_TELEPHONE="en_US"
LC_MEASUREMENT="en_US"
LC_IDENTIFICATION="en_US"
LC_ALL=
[centos@backend1 instagram]$ 

[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram  
[centos@backend1 instagram]$ date
Thu Dec 22 03:27:43 PST 2016 
[centos@backend1 instagram]$ sh -x  generate-incrementl_encfanid.sh
++ echo 'select tablename from entity_fan_meta where status = '\''merged'\'' and in_new_warehouse <> '\''YES'\'''
++ mysql -uroot -pindie -hlocalhost -A instagram_crawl -N
+ TABLES=instagram_crawl.entity_fan_2016dec21
+ OUTDIR=/mnt/gonguradrive/instagram
+ echo 'truncate incremental_ig ;'
truncate incremental_ig ;
+ cat /mnt/gonguradrive/instagram/premerge.sql
+ echo next
next
+ echo 'truncate incremental_ig ;'
+ cat /mnt/gonguradrive/instagram/premerge.sql
truncate incremental_ig ;
+ /home/centos/GonguraArchitecture/load-sql.sh /mnt/gonguradrive/instagram/premerge.sql
TRUNCATE TABLE and COMMIT TRANSACTION
+ '[' 0 -ne 0 ']'
+ echo 'Executing the Actual script '
Executing the Actual script  
+ for TT in '$TABLES'
++ date +%Y-%m-%d
++ echo instagram_crawl.entity_fan_2016dec21
++ sed -e s/instagram_crawl.entity_fan_//g
+ tee /mnt/gonguradrive/instagram/2016-12-22.log
+ T=2016dec21
+ echo 'Checking : /mnt/gonguradrive/instagram/2016dec21.sql.gz'
+ [[ ! -e /mnt/gonguradrive/instagram/2016dec21.sql.gz ]]
Checking : /mnt/gonguradrive/instagram/2016dec21.sql.gz
+ SEL='select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '\''|'\'', 1), '\''A1-'\'', '\'''\'') AS 
UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 2), '\''|'\'', -1), '\''A2-'\'', '\'''\'') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 3), '\''|'\'', -1), '\''A3-'\'', '\'''\'') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like '\''http:%'\'' then  REPLACE(REPLACE(post_link, '\''http://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') when post_link like '\''https:%'\'' then  REPLACE(REPLACE(post_link, '\''https://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'\'''\'') as polarity, ifnull(A.subjectivity,'\'''\'') as subjectivity from entity_fan_2016dec21 A INNER JOIN job_entities_2016dec21 B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid'
+ echo 'Working on 2016dec21:'
Working on 2016dec21:
+ echo 'select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '\''|'\'', 1), '\''A1-'\'', '\'''\'') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 2), '\''|'\'', -1), '\''A2-'\'', '\'''\'') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 3), '\''|'\'', -1), '\''A3-'\'', '\'''\'') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like '\''http:%'\'' then  REPLACE(REPLACE(post_link, '\''http://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') when post_link like '\''https:%'\'' then  REPLACE(REPLACE(post_link, '\''https://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'\'''\'') as polarity, ifnull(A.subjectivity,'\'''\'') as subjectivity from entity_fan_2016dec21 A INNER JOIN job_entities_2016dec21 B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid;'
select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from entity_fan_2016dec21 A INNER JOIN job_entities_2016dec21 B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid;
+ echo 'select /* SQL_NO_CACHE HIGH_PRIORITY */ B.aa_id as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '\''|'\'', 1), '\''A1-'\'', '\'''\'') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 2), '\''|'\'', -1), '\''A2-'\'', '\'''\'') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '\''|'\'', 3), '\''|'\'', -1), '\''A3-'\'', '\'''\'') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like '\''http:%'\'' then  REPLACE(REPLACE(post_link, '\''http://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') when post_link like '\''https:%'\'' then  REPLACE(REPLACE(post_link, '\''https://instagram.com/p/'\'', '\'''\''), '\''/'\'', '\'''\'') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'\'''\'') as polarity, ifnull(A.subjectivity,'\'''\'') as subjectivity from entity_fan_2016dec21 A INNER JOIN job_entities_2016dec21 B ON A.profile_id = B.profile_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON B.aa_id = E.entity_uid;'
+ mysql -u -n --quick -uroot -pindie -hlocalhost -A instagram_crawl
+ gzip -c
+ s3cmd put /mnt/gonguradrive/instagram/2016dec21.sql.gz s3://affinityanswers/incremental/instagram/
File '/mnt/gonguradrive/instagram/2016dec21.sql.gz' stored as 's3://affinityanswers/incremental/instagram/2016dec21.sql.gz' (143385096 bytes in 5.8 seconds, 23.59 MB/s) [1 of 1]  
+ '[' 0 -eq 0 ']'
+ echo success
success
+ INS='copy incremental_ig from '\''s3://affinityanswers/incremental/instagram/2016dec21.sql.gz'\'' credentials  '\''aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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'\'' delimiter '\''\t'\'' EMPTYASNULL GZIP IGNOREHEADER 1;'
+ echo 'BEGIN;'
+ echo 'copy incremental_ig from '\''s3://affinityanswers/incremental/instagram/2016dec21.sql.gz'\'' credentials  '\''aws_access_key_id=AKIAIHW4MJPABIICZO7Q;aws_secret_access_key=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'\'' delimiter '\''\t'\'' EMPTYASNULL GZIP IGNOREHEADER 1;'
+ echo 'COMMIT;'
+ /home/centos/GonguraArchitecture/load-sql.sh /mnt/gonguradrive/instagram/2016dec21.sql
BEGIN
psql:/mnt/gonguradrive/instagram/2016dec21.sql:2: INFO:  Load into table 'incremental_ig' completed, 8584189 record(s) loaded successfully.
COPY
COMMIT
+ '[' 0 -ne 0 ']'
+ echo 'UPDATE entity_fan_meta set in_new_warehouse = '\''YES'\'' where tablename = '\''instagram_crawl.entity_fan_2016dec21'\'';'
+ mysql -uroot -pindie -hlocalhost -A instagram_crawl
+ '[' 0 -ne 0 ']'
+ echo 'select count(*) from incremental_ig where create_time = 0;'
select count(*) from incremental_ig where create_time = 0;
+ echo 'select count(*) from incremental_ig where systemtime = 0;'
select count(*) from incremental_ig where systemtime = 0;
+ echo 'select count(*) from incremental_ig where profile_id = 0;'
select count(*) from incremental_ig where profile_id = 0;
+ echo 'select count(*) from incremental_ig where postid is null;'
select count(*) from incremental_ig where postid is null;
+ /home/centos/GonguraArchitecture/load-sql.sh /mnt/gonguradrive/instagram/postmerge.sql
+ '[' 0 -ne 0 ']'
[centos@backend1 instagram]$

MariaDB [instagram_crawl]> select * from entity_fan_meta where tablename = 'instagram_crawl.entity_fan_2016dec21';
+-----+--------------------------------------+--------+------------------+---------------------+
| id  | tablename                            | status | in_new_warehouse | loaded_at           |
+-----+--------------------------------------+--------+------------------+---------------------+
| 120 | instagram_crawl.entity_fan_2016dec21 | merged | YES              | 2016-12-22 04:55:28 |
+-----+--------------------------------------+--------+------------------+---------------------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from entity_fan_2016dec21;
+----------+
| count(*) |
+----------+
|  8584189 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]>

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=# select count(*) from events_ig;
   count   
-----------
 842887344
(1 row)

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=# select count(*) from incremental_ig where uuid = 0 or uuid is NULL or systemtime = 0 or systemtime is NULL or create_time = 0 or create_time is NULL or category = 0 or category is NULL or code1 = 0 or code1 is NULL;
 count 
-------
     0
(1 row)

gongura=# SELECT count(*) FROM INCREMENTAL_IG WHERE POSTID LIKE 'HTTP:%';
 count 
-------
     0
(1 row)

gongura=# insert into events_ig ( select * from incremental_ig);
INSERT 0 8584189
gongura=# select count(*) from events_ig;
   count   
-----------
 851471533
(1 row)

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=# select min(systemtime),max(systemtime) from incremental_ig;
    min     |    max     
------------+------------
 1482202989 | 1482371528
(1 row)

gongura=# 

[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram
[centos@backend1 instagram]$ sh -x getAndUpdateStats_aws.sh 2016dec21
+ '[' -z 2016dec21 ']'
+ week=2016dec21
+ OUTDIR=/mnt/gonguradrive/instagram
+ STATS='select profile_id , count(*) from events_ig group by profile_id order by profile_id asc ;'
+ echo '\o stats_2016dec21;'
+ echo '\t'
+ echo 'select profile_id , count(*) from events_ig group by profile_id order by profile_id asc ;'
+ echo '\t'
+ echo '\o ;'
+ /home/centos/GonguraArchitecture/load-sql.sh /mnt/gonguradrive/instagram/stats_2016dec21.sql
Showing only tuples.
Tuples only is off.
+ '[' 0 -ne 0 ']'
+ sqlcreatetblcmd='create table if not exists week_counts (profileId int, record_count int)'
+ mysql -u root -pindie -h localhost -D instagram_crawl -e 'create table if not exists week_counts (profileId int, record_count int);'
+ mysql -u root -pindie -h localhost -D instagram_crawl -e 'truncate week_counts;'
+ loadcmd='load data local infile '\''/mnt/gonguradrive/instagram/stats_2016dec21'\'' into table week_counts fields terminated by '\''|'\'' optionally enclosed by '\''"'\'' lines terminated by '\''\n'\'' ;'
+ mysql -u root -pindie -h localhost -D instagram_crawl -e 'load data local infile '\''/mnt/gonguradrive/instagram/stats_2016dec21'\'' into table week_counts fields terminated by '\''|'\'' optionally enclosed by '\''"'\'' lines terminated by '\''\n'\'' ;;'
+ mysql -hlocalhost -uinstagram -pinstagrampass -A instagram_statistics '-ealter ignore table weekly_profile_fan_counts add column 2016dec21 integer unsigned default null;'
+ mysql -hlocalhost -uinstagram -pinstagrampass -A instagram_statistics '-einsert ignore into weekly_profile_fan_counts (profile_uuid, 2016dec21) (select profileId, record_count from instagram_crawl.week_counts where profileId != 0 ) on duplicate key update 2016dec21 = VALUES(2016dec21);'
[centos@backend1 instagram]$

<mobydick || @loader3 2016dec21>$ bash  ../../launch-ec2-instance.sh delete
Deleting db with i-0056b0e51c5e39631 : [success] (log = db.terminate.log)
Removing Security Rules for i-0f6ac049556ce1e20 and 172.31.44.152 :  (port 3306 removed) (port 6379 removed)
Deleting crawl1 with i-0f6ac049556ce1e20 : [success] (log = crawl1.terminate.log)

Removing Security Rules for i-029dc5463087be833 and 172.31.41.20 :  (port 3306 removed) (port 6379 removed)
Deleting crawl2 with i-029dc5463087be833 : [success] (log = crawl2.terminate.log)

Removing Security Rules for i-0100b6339a9cd8d66 and 172.31.46.203 :  (port 3306 removed) (port 6379 removed)
Deleting crawl3 with i-0100b6339a9cd8d66 : [success] (log = crawl3.terminate.log)

Removing Security Rules for i-0d298609c93853f6f and 172.31.35.137 :  (port 3306 removed) (port 6379 removed)
Deleting crawl4 with i-0d298609c93853f6f : [success] (log = crawl4.terminate.log)

Removing Security Rules for i-058d74ecfff488685 and 172.31.46.182 :  (port 3306 removed) (port 6379 removed)
Deleting crawl5 with i-058d74ecfff488685 : [success] (log = crawl5.terminate.log)



crawl1 172.31.44.152 35.163.248.77
crawl2 172.31.41.20 35.166.120.201
crawl3 172.31.46.203 35.166.83.35
crawl4 172.31.35.137 35.165.113.190
crawl5 172.31.46.182 35.166.51.37
db 172.31.4.10 35.162.178.226
<mobydick || @loader3 2016dec21>$

[centos@backend1 instagram]$ pwd
/mnt/gonguradrive/instagram
[centos@backend1 instagram]$ sh -x generate-instagram-crawl-error-report-email.sh 2016dec21
+ '[' -z 2016dec21 ']'
+ catalog=2016dec21
+ TO=operations@affinityanswers.com
+ CC=ananth@affinityanswers.com,kavithap@affinityanswers.com,soundar@affinityanswers.com,maheshn@affinityanswers.com,saikumar@affinityanswers.com
+ mysql -uinstagram -pinstagrampass -hlocalhost -A -t '-eselect aa_id as '\''S.No'\'', category, profile_id, url, crawl_code from job_entities_2016dec21 where crawl_code not like '\''200%'\''' instagram_crawl
+ mail -s '2016dec21 - instagram :: crawl errors report' -c ananth@affinityanswers.com,kavithap@affinityanswers.com,soundar@affinityanswers.com,maheshn@affinityanswers.com,saikumar@affinityanswers.com operations@affinityanswers.com
+ mysql -uinstagram -pinstagrampass -hlocalhost -A -t '-eselect aa_id as '\''S.No'\'', category, profile_id, url, crawl_code from job_entities_2016dec21 where status = '\''crawled'\'' and  num_http_reqs = 0 and num_posts = 0 ' instagram_crawl
+ mail -s '2016dec21 - instagram :: crawl private profiles report' -c ananth@affinityanswers.com,kavithap@affinityanswers.com,soundar@affinityanswers.com,maheshn@affinityanswers.com,saikumar@affinityanswers.com operations@affinityanswers.com
+ mail -s '2016dec21 - instagram :: crawl profiles with ALL posts above MAX POST AGE' -c ananth@affinityanswers.com,kavithap@affinityanswers.com,soundar@affinityanswers.com,maheshn@affinityanswers.com,saikumar@affinityanswers.com operations@affinityanswers.com
+ mysql -uinstagram -pinstagrampass -hlocalhost -A -t '-eselect aa_id as '\''S.No'\'', category, profile_id, url, crawl_code from job_entities_2016dec21 where status = '\''crawled'\'' and  num_http_reqs <> 0 and num_posts = 0 ' instagram_crawl
[centos@backend1 instagram]$

gongura=# select count(*) from events_ig;
   count   
-----------
 851471533
(1 row)

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=#

MariaDB [instagram_crawl]> select count(*) from all_networks.UUID_ig;
+-----------+
| count(*)  |
+-----------+
| 106528379 |
+-----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select max(uuid) from all_networks.UUID_ig;
+-----------+
| max(uuid) |
+-----------+
| 106528379 |
+-----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from all_networks.UUID_ig where last_updt_time > '2016-12-14';
+----------+
| count(*) |
+----------+
|  1368460 |
+----------+
1 row in set (1.09 sec)

MariaDB [instagram_crawl]> select count(*) from all_networks.UUID_ig where last_updt_time > '2016-12-15';
+----------+
| count(*) |
+----------+
|   677007 |
+----------+
1 row in set (0.42 sec)

MariaDB [instagram_crawl]>

gongura=# select * from ig_weekly_summary where cat_week = '2016-12-14';
  cat_week  | crawl_starttime | crawl_endtime | total_ef_count | total_uuid_count | max_uuid  | count_new_uuids | total_entities | err_entity_404 | er
r_entity_private | new_fans_percentage | total_records_in_master 
------------+-----------------+---------------+----------------+------------------+-----------+-----------------+----------------+----------------+---
-----------------+---------------------+-------------------------
 2016-12-14 |      1481542176 |    1481696897 |        8606260 |        105851372 | 105851372 |          691453 |          22967 |             29 |   
              39 |              8.0343 |               842887344
(1 row)

gongura=#

MariaDB [instagram_crawl]> alter table instagram_universe.post rename to instagram_universe.post_2016dec21;
Query OK, 0 rows affected (0.01 sec)

MariaDB [instagram_crawl]>

[colligent@backend1 ~]$ cd /home/colligent/instagram-crossposting/
[colligent@backend1 instagram-crossposting]$
[colligent@backend1 instagram-crossposting]$ pwd
/home/colligent/instagram-crossposting
[colligent@backend1 instagram-crossposting]$
[colligent@backend1 instagram-crossposting]$ sh getcrossusers.sh 2016dec21
Getting the Instagram Handles from Entity_catalog.txt
Getting the crawled posts from DB
    Loading the Excluded Entities
ERROR 1064 (42000) at line 1: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'ID)' at line 1
    Extracting the comment_id, message of the included entities comments with @ symbol
Getting the Matched Instagram Handles with comment messages
Loading the Matched Data into the Table
Get the other fields in compact table
Explode the compact table for  Commenters for that comments
Completed
[colligent@backend1 instagram-crossposting]$

MariaDB [instagram_crawl]> select count(*) from cross_comment_exploded_2016dec21;
+----------+
| count(*) |
+----------+
|    18185 |
+----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from cross_comment_exploded_2016dec21 where src_entity_uid is null OR dest_entity_uid is NULL OR userid is null OR fan_relationship is null OR comment_id is null OR created_time is NULL;
+----------+
| count(*) |
+----------+
|        0 |
+----------+
1 row in set (0.02 sec)

MariaDB [instagram_crawl]> select count(*) from cross_comment_exploded_2016dec21 where polarity is NULL or subjectivity is NULL;
+----------+
| count(*) |
+----------+
|        0 |
+----------+
1 row in set (0.01 sec)

MariaDB [instagram_crawl]> select min(fetch_time), max(fetch_time) from cross_comment_exploded_2016dec21;
+---------------------+---------------------+
| min(fetch_time)     | max(fetch_time)     |
+---------------------+---------------------+
| 2016-12-19 19:03:19 | 2016-12-21 17:50:13 |
+---------------------+---------------------+
1 row in set (0.01 sec)

MariaDB [instagram_crawl]> select min(fetch_time), max(fetch_time) from cross_comment_exploded_all;
+---------------------+---------------------+
| min(fetch_time)     | max(fetch_time)     |
+---------------------+---------------------+
| 2015-11-22 19:51:51 | 2016-12-13 22:21:39 |
+---------------------+---------------------+
1 row in set (1.97 sec)

MariaDB [instagram_crawl]> alter table cross_comment_exploded_2016dec21 rename to cross_comment_exploded_2016dec21_backup_before_dedup;
Query OK, 0 rows affected (0.00 sec)

MariaDB [instagram_crawl]> insert  ignore into cross_comment_exploded_all select * from   cross_comment_exploded_2016dec21_backup_before_dedup;
Query OK, 7778 rows affected, 10407 warnings (4.30 sec)
Records: 18185  Duplicates: 10407  Warnings: 10407

MariaDB [instagram_crawl]> create table cross_comment_exploded_2016dec21  as select * from cross_comment_exploded_all  where fetch_time >= ( select min(fetch_time) from cross_comment_exploded_2016dec21_backup_before_dedup );
Query OK, 7778 rows affected (0.77 sec)
Records: 7778  Duplicates: 0  Warnings: 0

MariaDB [instagram_crawl]> insert into entity_fan_meta ( tablename, status, in_new_warehouse ) values ('cross_comment_exploded_2016dec21' , 'merged' , 'NO' );
Query OK, 1 row affected (0.01 sec)

MariaDB [instagram_crawl]>

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=# delete from incremental_ig;
DELETE 8584189
gongura=# select count(*) from incremental_ig;
 count 
-------
     0
(1 row)

gongura=#

[colligent@backend1 instagram-crossposting]$ export LC_CTYPE="en_US.UTF-8"
[colligent@backend1 instagram-crossposting]$ 
[colligent@backend1 instagram-crossposting]$ sh generate-incrementl_encfanid-crosspost.sh
truncate incremental_ig ;
next
truncate incremental_ig ;
TRUNCATE TABLE and COMMIT TRANSACTION
Executing the Actual script 
tee: /mnt/gonguradrive/instagram/2016-12-22.log: Permission denied
table : cross_comment_exploded_2016dec21
DATE : 2016dec21
DataFile : 2016dec21.xcommentsql.gz
QueryFile : 2016dec21.xcomment.sql
DATA SQL [2016dec21.xcommentsql.gz] 
Checking For Already existing : [/mnt/gonguradrive/instagram/2016dec21.xcommentsql.gz] 
Working on cross_comment_exploded_2016dec21:
select A.dest_entity_uid as profile_id, CAST(REPLACE(SUBSTRING_INDEX(E.attrib, '|', 1), 'A1-', '') AS UNSIGNED) as A1, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 2), '|', -1), 'A2-', '') AS UNSIGNED) as A2, CAST(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(E.attrib, '|', 3), '|', -1), 'A3-', '') AS UNSIGNED) as A3, C.id as category, 0 as userid,D.id as fan_relationship,IF(A.created_time = 0, UNIX_TIMESTAMP(A.fetch_time), A.created_time) created_time, CASE when post_link like 'http:%' then  REPLACE(REPLACE(post_link, 'http://instagram.com/p/', ''), '/', '') when post_link like 'https:%' then  REPLACE(REPLACE(post_link, 'https://instagram.com/p/', ''), '/', '') END as post_id , 0 as gender, 0 as age, 0 as market, 0 as ethinicity, A.comment_id as commentid, UNIX_TIMESTAMP(A.fetch_time) as fetch_time, extract_or_create_uuid(A.userid) as uuid , ifnull(A.polarity,'') as polarity, ifnull(A.subjectivity,'') as subjectivity from cross_comment_exploded_2016dec21 A INNER JOIN job_entities_2016dec21 B ON A.dest_entity_uid = B.aa_id INNER JOIN category_map C ON B.category = C.code INNER JOIN relationship_map D ON D.rel = A.fan_relationship INNER JOIN all_networks.validation_catalog E ON A.dest_entity_uid = E.entity_uid;
File '/mnt/gonguradrive/instagram/2016dec21.xcommentsql.gz' stored as 's3://affinityanswers/incremental/instagram/2016dec21.xcommentsql.gz' (249938 bytes in 0.1 seconds, 2.88 MB/s) [1 of 1]
success
 Creating the QueySQL to Load the data into redshift 
 Executing the QuerySQL to Load the data into redshift 
BEGIN
psql:/mnt/gonguradrive/instagram/2016dec21.xcomment.sql:2: INFO:  Load into table 'incremental_ig' completed, 7770 record(s) loaded successfully.
COPY
COMMIT
select count(*) from incremental_ig where create_time = 0;
select count(*) from incremental_ig where systemtime = 0;
select count(*) from incremental_ig where profile_id = 0;
select count(*) from incremental_ig where postid is null;
[colligent@backend1 instagram-crossposting]$

gongura=# select count(*) from incremental_ig;
 count 
-------
  7770
(1 row)

gongura=# select min(systemtime) , max(systemtime) from incremental_ig;
    min     |    max     
------------+------------
 1482202999 | 1482371413
(1 row)

[colligent@loader3 ~]$ date -d@1482202999
Mon Dec 19 19:03:19 PST 2016
[colligent@loader3 ~]$ 
[colligent@loader3 ~]$ date -d@1482371413
Wed Dec 21 17:50:13 PST 2016
[colligent@loader3 ~]$

gongura=# select count(*) from incremental_ig;
 count 
-------
  7770
(1 row)

gongura=# select min(systemtime) , max(systemtime) from incremental_ig;
    min     |    max     
------------+------------
 1482202999 | 1482371413
(1 row)

gongura=# select fan_relationship , count(*) from incremental_ig group by fan_relationship;
 fan_relationship | count 
------------------+-------
               64 |  7770
(1 row)

gongura=# select profile_id , count(*) from incremental_ig where fan_relationship in (16,32,64) group by profile_id order by count(*) desc limit 5;
 profile_id | count 
------------+-------
      22478 |    81
       4425 |    71
       2139 |    66
      40573 |    57
      39558 |    53
(5 rows)

gongura=# insert into events_ig select * from incremental_ig;
INSERT 0 7770
gongura=# select count(*) from events_ig where fan_relationship in (16,32,64);
  count  
---------
 2789201
(1 row)

gongura=# select count(*) from events_ig;
   count   
-----------
 851479303
(1 row)

gongura=# select count(*) from events_ig;
   count   
-----------
 851479303
(1 row)

gongura=# select count(*) from incremental_ig;
  count  
---------
 8584189
(1 row)

gongura=# select count(*) from incremental_ig;
 count 
-------
  7770
(1 row)

gongura=#

MariaDB [instagram_crawl]> select count(*) from all_networks.UUID_ig;
+-----------+
| count(*)  |
+-----------+
| 106528379 |
+-----------+
1 row in set (0.01 sec)

MariaDB [instagram_crawl]> select max(uuid) from all_networks.UUID_ig;
+-----------+
| max(uuid) |
+-----------+
| 106528379 |
+-----------+
1 row in set (0.00 sec)

MariaDB [instagram_crawl]> select count(*) from all_networks.UUID_ig where last_updt_time > '2016-12-15';
+----------+
| count(*) |
+----------+
|   677007 |
+----------+
1 row in set (0.46 sec)

MariaDB [instagram_crawl]>

gongura=# select count(distinct profile_id) from events_ig where fan_relationship in (16,32,64)  and systemtime >= 1482202999 and systemtime <= 1482371413;
 count 
-------
  2492
(1 row)

gongura=#

gongura=# insert into ig_weekly_summary (cat_week , crawl_starttime , crawl_endtime , total_ef_count , total_uuid_count , max_uuid , count_new_uuids , total_entities , err_entity_404 , err_entity_private , new_fans_percentage, total_records_in_master) values ('2016-dec-21',  1482202989, 1482371528, 8584189, 106528379, 106528379, 677007, 23030, 80,24,7.88667,851479303);
INSERT 0 1
gongura=# select * from ig_weekly_summary where cat_week = '2016-12-21';
  cat_week  | crawl_starttime | crawl_endtime | total_ef_count | total_uuid_count | max_uuid  | count_new_uuids | total_entities | err_entity_404 | er
r_entity_private | new_fans_percentage | total_records_in_master 
------------+-----------------+---------------+----------------+------------------+-----------+-----------------+----------------+----------------+---
-----------------+---------------------+-------------------------
 2016-12-21 |      1482202989 |    1482371528 |        8584189 |        106528379 | 106528379 |          677007 |          23030 |             80 |   
              24 |             7.88667 |               851479303
(1 row)

gongura=#
