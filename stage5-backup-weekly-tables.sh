#!/bin/bash

# Script to take backup of existing data from the stage5 instagram tables

{
    if [[ -z $1 || -z $2 ]];then
        echo "syntax: $0 <catalog-week> <out-dir>"
        exit 0;
    fi

    WEEK=$1
    OUT_DIR=$2
    FULL="$OUT_DIR/$WEEK/stage5"

    echo "Starting backups on stage5 database - $(date)"

    mkdir -p "$FULL" && cd "$FULL"

    # Take backup of UUID table
    echo -e "Taking backup for UUID_ig : \c"
    OUT=$FULL/all_networks.UUID_ig.sql
    mysqldump -uroot -pindie -hstage5 all_networks UUID_ig > "$OUT"
    if [ $? -ne 0 ];then
        echo "[FAILED]";
        echo "Backup For '$OUT' Failed. Manual intervention needed ...."
        exit 1
    else
        echo "[SUCCESS] - `du -hs $OUT | awk '{print \$1}'`"
    fi

    # Take backup of master tables
    for C in ART BRN CEL DIG GAM LOC MAG MOV RAD SPR TVS
    do
        T=entity_fan_${C}_master
        D=instagram
        echo -e "Taking backup for $T : \c"
        OUT=$FULL/$D.$T.sql
        mysqldump -uinstagram -pinstagrampass -hstage5 $D $T > "$OUT"
        if [ $? -ne 0 ];then
            echo "[FAILED]";
            echo "Backup For '$OUT' Failed. Manual intervention needed ...."
            exit 1
        else
            echo "[SUCCESS] - `du -hs $OUT | awk '{print \$1}'`"
        fi
    done

    # Take backup of UUID table
    echo -e "Taking backup for fan_universe : \c"
    OUT=$FULL/instagram_universe.fan_universe.sql
    mysqldump -uroot -pindie -hstage5 instagram_universe fan_universe > "$OUT"
    if [ $? -ne 0 ];then
        echo "[FAILED]";
        echo "Backup For '$OUT' Failed. Manual intervention needed ...."
        exit 1
    else
        echo "[SUCCESS] - `du -hs $OUT | awk '{print \$1}'`"
    fi

    # Compress the backup files
    echo "Compressing all the files under $FULL";
    ls -l $FULL
    bzip2 $FULL/*.sql

    echo "All done - $(date)"
} 2>&1 | tee -a "stage5-weeklybackup-$(date).log"
