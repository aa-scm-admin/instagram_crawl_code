package InstagramConstants;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(STATUS_NEW STATUS_CRAWLING STATUS_CRAWLED STATUS_MERGING STATUS_MERGED shortenPostLink expandPostLink);

use constant STATUS_NEW      => 'new';
use constant STATUS_CRAWLING => 'crawling';
use constant STATUS_CRAWLED  => 'crawled';
use constant STATUS_MERGING  => 'merging';
use constant STATUS_MERGED   => 'merged';

sub shortenPostLink {
    my $link = shift;
    return '' if not defined $link;
    $link =~ s/^http:\/\/instagram.com/ig::/g;
    return $link;
}

sub expandPostLink {
    my $link = shift;
    return '' if not defined $link;
    $link =~ s/^ig::/^http:\/\/instagram.com/g;
    return $link;
}

1;
