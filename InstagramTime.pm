package InstagramTime;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Simple Time functions
# Date: July/2014


require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(convert_time);

use utf8;
use POSIX qw(strftime);

sub convert_time {
  my $time = shift;
  my $days = int($time / 86400);
  $time -= ($days * 86400);
  my $hours = int($time / 3600);
  $time -= ($hours * 3600);
  my $minutes = int($time / 60);
  my $seconds = $time % 60;

  $seconds = $seconds < 1 ? '' : $seconds . 's';
  $days = $days < 1 ? '' : $days .'d ';
  $hours = $hours < 1 ? '' : $hours .'h ';
  $minutes = $minutes < 1 ? '' : $minutes . 'm ';
  $time = $days . $hours . $minutes . $seconds;
  $time =~ s/^\s+|\s+$//g;
  return $time;
}
