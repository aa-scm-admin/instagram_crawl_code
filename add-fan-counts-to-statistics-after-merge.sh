#!/bin/bash

#
# Script to add the number of fans crawled for each profile
# in a given catalog week
#

if [ -z $1 ];then
    echo "syntax: $0 <catalog-week>";
    exit;
fi

week=$1

mysql -hstage5 -uinstagram -pinstagrampass -A instagram_statistics -e"alter ignore table weekly_profile_fan_counts add column $week integer unsigned default null;";

mysql -hstage5 -uinstagram -pinstagrampass -A instagram_statistics -e"insert ignore into weekly_profile_fan_counts (profile_uuid, $week) (select entity_uid, count(*) from instagram.entity_fan_ALL_master group by entity_uid) on duplicate key update $week = VALUES($week);";

