package InstagramLog;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Logger Interface
# Date: May/2014

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(w_debug w_info w_warn w_error w_crit w_exit);

use utf8;
use POSIX qw(strftime);
use JSON;

use constant LOG_DEBUG => (0x0001 << 1);
use constant LOG_INFO  => (0x0001 << 2);
use constant LOG_WARN  => (0x0001 << 3);
use constant LOG_ERROR => (0x0001 << 4);
use constant LOG_CRIT  => (0x0001 << 5);
use constant LOG_ALL   => (LOG_DEBUG | LOG_INFO | LOG_WARN | LOG_ERROR | LOG_CRIT);
use constant LOG_PROD  => LOG_ALL & ~LOG_DEBUG;

my $loglevel  = defined $ENV{LOGLEVEL} ? $ENV{LOGLEVEL} : LOG_PROD;

sub w_log($$$) {
    my ($level, $message, $profile, $category) = @_;
    my ($sleep_secs_profile, $sleep_secs_posts) = (0, 0);
    my $pid       = $$;
    my $logtype   = (defined $ENV{LOG_FORMAT} and $ENV{LOG_FORMAT} eq 'json') ? 'json' : 'plain';
    $category = '...' if not defined $category;
    $profile  = ''    if not defined $profile;
    $message  = ''    if not defined $message;
    $sleep_secs_profile = defined $ENV{SLEEP_SECS_PROFILE} ? $ENV{SLEEP_SECS_PROFILE} : 0;
    $sleep_secs_posts   = defined $ENV{SLEEP_SECS_POSTS} ? $ENV{SLEEP_SECS_POSTS} : 0;
    if ($logtype ne 'json') {
        printf(
            "%d %s [%-19s] (%1.1fs/%1.1fs) %3s %s : %s\n",
            $pid,
            $level,
            strftime("%F %T", localtime),
            $sleep_secs_profile, $sleep_secs_posts,
            $category, $profile,
            $message
        ) if defined $level;
    } else {
        my $line = {
            'pid'                => $pid,
            'level'              => $level,
            'time'               => strftime("%F %T", localtime),
            'sleep_secs_profile' => $sleep_secs_profile,
            'sleep_secs_post'    => $sleep_sec_posts,
            'category'           => $category,
            'profile'            => $profile,
            'message'            => $message
        };
        print(encode_json($line)) if defined $level;
    }
}

sub w_debug($$$)  { &w_log("DEBUG",  shift, shift, shift) if $loglevel & LOG_DEBUG; }
sub w_info($$$)   { &w_log("INFO",  shift, shift, shift) if $loglevel & LOG_INFO; }
sub w_warn($$$)   { &w_log("WARN",  shift, shift, shift) if $loglevel & LOG_WARN; }
sub w_error($$$)  { &w_log("ERROR", shift, shift, shift) if $loglevel & LOG_ERROR; }
sub w_crit($$$)   { &w_log("CRIT",  shift, shift, shift) if $loglevel & LOG_CRIT; }
sub w_exit($$$)   { &w_log("EXIT", shift, shift); exit; }

1;
