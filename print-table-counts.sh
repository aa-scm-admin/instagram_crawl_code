#!/bin/bash

for CAT in `mysql -uinstagram -hstage5 -A instagram -pinstagrampass -e'show tables like "%master%"' | grep master | cut -d_ -f3 | grep -v ^insta | grep -v ALL`
do
    table=instagram.entity_fan_${CAT}_master
    echo "count from $table ::"
    echo "select count(*) as total from $table" | mysql -t -uinstagram -hstage5 -pinstagrampass -A
done

for table in all_networks.UUID_ig instagram_universe.fan_universe
do
    echo "count from $table ::"
    echo "select count(*) as total from $table" | mysql -t -uinstagram -hstage5 -pinstagrampass -A
done

echo "count from common_profile_counts";
echo "select count(*) as common_profile_counts from instagram.common_profile_counts" | mysql -t -uinstagram -hstage5 -pinstagrampass -A

echo "count from common_profile_counts (total_profiles > 1)";
echo "select count(*) as total_profiles_gt_1 from instagram.common_profile_counts where total_profiles > 1" | mysql -t -uinstagram -hstage5 -pinstagrampass -A

echo "count from common_fan_counts";
echo "select count(*) as common_fan_counts from instagram.common_fan_counts" | mysql -t -uinstagram -hstage5 -pinstagrampass -A

echo "count from common_fan_counts (total_fans > 40)";
echo "select count(*) as common_fan_counts_gt_40 from instagram.common_fan_counts where total_fans > 40" | mysql -t -uinstagram -hstage5 -pinstagrampass -A
