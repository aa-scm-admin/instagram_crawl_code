package InstagramCrawlDB;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Database interaction layer for Crawling
# Date: May/2014

use utf8;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use InstagramLog;
use InstagramConstants;
use Sys::Hostname;

my $debug    = defined $ENV{DEBUG} ? 1 : 0;
my $verbose  = defined $ENV{VERBOSE} ? 1 : 0;
my $show_sql = defined $ENV{SHOW_SQL} ? 1 : 0;

sub new {
    my $class = shift;
    my $config= shift;
    my $self  = {
        _dbh        => undef,
        _config     => $config,
        _myname     => hostname,
        _username   => $config->{db}->{crawl}->{user},
        _password   => $config->{db}->{crawl}->{pass},
        _hostname   => $config->{db}->{crawl}->{host},
        _dbcrawl    => $config->{db}->{crawl}->{db},
        _prefix     => $config->{db}->{crawl}->{prefix},
        _workmode   => $config->{global}->{workmode},
        _dbuniverse => $config->{db}->{universe}->{db},
    };
    print Dumper($self) if $debug;
    $self->{_myname} =~ s/\.(colligent|affinityanswers)\.com//;
    bless($self, $class);    # make $self an object of class $class
    return $self;
}

sub getDBH {
    my $self = shift;
    return $self->{_dbh};
}

sub getTableName($) {
    my $self  = shift;
    my $name  = shift;
    my $table = $self->{_config}->{tables}->{$name};
    if (not defined $table) {
        &w_exit("'$name' is not found in Instagram.json. Aborting");
    }
    return $table;
}

sub setup() {
    my $self = shift;
    $self->{_dbh}      = DBI->connect(
        'DBI:mysql:database='.$self->{_dbcrawl}.';host='.$self->{_hostname},
        $self->{_username},
        $self->{_password}
    ) || &w_exit("Error: Could not connect to database: $DBI::errstr");

    # Set the sql strict mode
    $self->{_dbh}->do("set sql_mode='STRICT_ALL_TABLES'") || &w_exit("Error: Failed to set strict mode.");

    # create the session variable to store the key
    # Without this variable, all values encrypted will be just NULLs
    $self->{_dbh}->do("set \@aa_key='".$self->{_config}->{hex_key}."';") || &w_exit("Error: Failed to set key.");
}

sub addPost() {
    my $self       = shift;
    my $profile_id = shift;
    my $post_id    = shift;
    my $url        = shift;
    my $title      = shift;
    my $sql = sprintf "INSERT INTO ".$self->getTableName('post')." (profile_id, post_id, url, title, createdAt) VALUES (%s, %s, %s, %s, now()) ON DUPLICATE KEY UPDATE url = %s, title = %s, updatedAt = now()", $self->{_dbh}->quote($profile_id), $self->{_dbh}->quote($post_id), $self->{_dbh}->quote($url), $self->{_dbh}->quote($title), $self->{_dbh}->quote($url), $self->{_dbh}->quote($title);
    &w_debug("$sql") if $show_sql;
    my $a = $self->{_dbh}->do($sql) || &w_exit($DBI::errstr);
    my $b = $self->{_dbh}->{'mysql_insertid'};
    return $b;
}

sub addCommentToPost($$$) {
    my $self       = shift;
    my $profile_id = shift;
    my $user_aaid  = shift;
    my $post_aaid  = shift;
    my $comment    = shift;
    my $sql = sprintf "INSERT INTO ".$self->getTableName('postcomment')." (user_aaid, post_aaid, message, comment_id, created_time, fetch_time) VALUES (%s, %s, %s, %s, %s, now())", $self->{_dbh}->quote($user_aaid), $self->{_dbh}->quote($post_aaid), $self->{_dbh}->quote($comment->{text}), $self->{_dbh}->quote($comment->{id}), $self->{_dbh}->quote($comment->{created_time});
    &w_debug("$sql") if $show_sql;
    my $a = $self->{_dbh}->do($sql) || &w_exit($DBI::errstr);
    my $b = $self->{_dbh}->{'mysql_insertid'};
    return $b;
}

# hex(aes_encrypt(fanhandle, unhex(@aa_key))
# aes_decrypt(unhex(fanhandle),unhex(@aa_key));
sub addUser($$$) {
    my $self       = shift;
    my $profile_id = shift;
    my $from       = shift;
    my $sql        = sprintf "SELECT aa_id from ".$self->getTableName('user')." where nw_id = hex(aes_encrypt(%s, unhex(\@aa_key)))", $self->{_dbh}->quote($from->{id});
    my $sth        = $self->{_dbh}->prepare($sql);
    &w_exit($sql) if (!$sth);
    $sth->execute() || &w_exit($DBI::errstr);
    my @result = $sth->fetchrow_array();
    my $return = [];
    my ($a, $b);
    if (defined $result[0]) {
        # User already exists in the system
        &w_debug("User ".$from->{username}." already exists with id".$result[0], $profile_id);
        $return = [0, $result[0]];
        # Update the attributes of the user, in case they have changed.
        $sql = sprintf "UPDATE ".$self->getTableName('user')." SET username = hex(aes_encrypt(%s, unhex(\@aa_key))), profile_picture = %s, full_name = %s, updatedAt = now() where aa_id = %s", $self->{_dbh}->quote($from->{username}), $self->{_dbh}->quote($from->{profile_picture}), $self->{_dbh}->quote($from->{full_name}), $self->{_dbh}->quote($result[0]);
        &w_debug("$sql") if $show_sql;
        $a = $self->{_dbh}->do($sql);
        if (!$a) {
            &w_error($sql, $profile_id);
            &w_exit("Aborting due to SQL Failure.");
        }
    } else {
        # Insert the user into the system
        $sql = sprintf "INSERT INTO ".$self->getTableName('user')." (username, profile_picture, nw_id, full_name, createdAt) VALUES (hex(aes_encrypt(%s, unhex(\@aa_key))), %s, hex(aes_encrypt(%s, unhex(\@aa_key))), %s, now())", $self->{_dbh}->quote($from->{username}), $self->{_dbh}->quote($from->{profile_picture}), $self->{_dbh}->quote($from->{id}), $self->{_dbh}->quote($from->{full_name});
        &w_debug("addUser:: $sql");
        $a = $self->{_dbh}->do($sql);
        if (!$a) {
            &w_error($sql, $profile_id);
            &w_exit("Aborting due to SQL Failure.");
        }
        $b = $self->{_dbh}->{'mysql_insertid'};
        &w_debug("Added user ".$from->{username}." with id ".$b, $profile_id);
        $return = [1, $b];
    }
    return $return;
}

sub fetchProfileToCrawl {
    my $self     = shift;
    my $category = shift;
    my $table    = $self->getTableName('job_entities');
    my $crawl_order = "ORDER BY max_post_age DESC";
    if ($self->{_config}->{crawl_order} eq 'newest_first') {
        $crawl_order = "ORDER BY max_post_age ASC";
    }
    my $sql      = sprintf "SELECT profile_id, category, unix_timestamp(date('%s')) - unix_timestamp(catalog_week) as catalog_diff_secs, max_post_age from %s where status = %s $crawl_order LIMIT 1", $self->{_config}->{current_catalog_week}, $table, $self->{_dbh}->quote(STATUS_NEW);
    if (defined $category and length($category) == 3) {
        $sql  = sprintf "SELECT profile_id, category, unix_timestamp(date('%s')) - unix_timestamp(catalog_week) as catalog_diff_secs, max_post_age from %s where status = %s and category = %s $crawl_order LIMIT 1", $self->{_config}->{current_catalog_week}, $table, $self->{_dbh}->quote(STATUS_NEW), $self->{_dbh}->quote($category);
    }
    $self->{_dbh}->do('LOCK TABLES '.$table.' WRITE');
    my $sth = $self->{_dbh}->prepare($sql);
    $sth->execute() || &w_exit($sql." ==> ".$DBI::errstr);
    my @result = $sth->fetchrow_array();
    my $profile_id;
    if (defined $result[0]) {
        $profile_id = $result[0];
        # Update the status of this profile
        $sql = sprintf "UPDATE %s set crawl_pid = %d, status = %s, crawl_hostname = %s, crawl_start_time = now(), crawl_end_time = NULL, num_posts = 0, num_users = 0, num_comments = 0 WHERE profile_id = %s", $table, $$, $self->{_dbh}->quote(STATUS_CRAWLING), $self->{_dbh}->quote($self->{_myname}), $self->{_dbh}->quote($profile_id);
        &w_info($sql, $profile_id) if $show_sql;
        $self->{_dbh}->do($sql) || &w_exit("$DBI::errstr");
    }
    $self->{_dbh}->do('UNLOCK TABLES');
    return @result;
}

sub markProfileCrawled {
    my $self          = shift;
    my $profile       = shift;
    my $profile_id    = $profile->[0];
    my $profile_cat   = $profile->[1];
    my $profile_age   = $profile->[2];
    my $max_post_age  = $profile->[3];
    my $metrics       = shift;
    my $crawl_code    = '200 OK';
    my $crawl_status  = STATUS_CRAWLED;
    my $num_http_reqs = $metrics->{num_http_reqs};

    if ($metrics->{crawl_code} ne '200 OK') {
        $crawl_code    = substr($metrics->{crawl_code}, 0, 16);
        $num_http_reqs = 1;
    }

    # Update the status of this profile
    my $sql = sprintf "UPDATE ".$self->getTableName('job_entities')." set num_http_reqs = %s, crawl_code = %s, status = %s, crawl_end_time = now(), crawl_pid = %d WHERE profile_id = %s", $self->{_dbh}->quote($num_http_reqs), $self->{_dbh}->quote($crawl_code), $self->{_dbh}->quote($crawl_status), $$, $self->{_dbh}->quote($profile_id);
    &w_info($sql, $profile_id, $profile_cat) if $show_sql;
    &w_info("Crawl is complete for the profile.", $profile_id, $profile_cat);
    my $rows = $self->{_dbh}->do($sql) || &w_exit("$DBI::errstr");
    return $rows;
}

sub updateStatistics {
    my $self             = shift;
    my $profile_id       = shift;
    my $profile_cat      = shift;
    my $metrics          = shift;
    my $user_stats       = shift;
    my $show_stats       = shift;
    my $num_posts        = $metrics->{posts};
    my $num_users        = $metrics->{users}->{global}->{new} + $metrics->{users}->{global}->{old};
    my $num_comments     = $metrics->{image_comments};
    my $num_likes        = $metrics->{image_likes};
    my $vid_comments     = $metrics->{video_comments};
    my $vid_likes        = $metrics->{video_likes};
    my $num_http_reqs    = $metrics->{num_http_reqs};
    my $glob_posts       = $user_stats->{counts}->{media};
    my $glob_follows     = $user_stats->{counts}->{follows};
    my $glob_followed_by = $user_stats->{counts}->{followed_by};

    # To avoid storing null values in the database
    $glob_posts       = defined $glob_posts ? $glob_posts : 0;
    $glob_follows     = defined $glob_follows ? $glob_follows : 0;
    $glob_followed_by = defined $glob_followed_by ? $glob_followed_by : 0;

    my $sql = sprintf "UPDATE ".$self->getTableName('job_entities')." set num_http_reqs = %s, crawl_end_time = now(), vid_comments = %s, vid_likes = %s, global_posts = %s, global_followers = %s, global_following = %s, num_likes = %s, num_posts = %s, num_users = %s, num_comments = %s WHERE profile_id = %s", $self->{_dbh}->quote($num_http_reqs), $self->{_dbh}->quote($vid_comments), $self->{_dbh}->quote($vid_likes), $self->{_dbh}->quote($glob_posts), $self->{_dbh}->quote($glob_follows), $self->{_dbh}->quote($glob_followed_by), $self->{_dbh}->quote($num_likes), $self->{_dbh}->quote($num_posts), $self->{_dbh}->quote($num_users), $self->{_dbh}->quote($num_comments), $self->{_dbh}->quote($profile_id);
    &w_info($sql, $profile_id, $profile_cat) if $show_sql;
    $self->{_dbh}->do($sql) || &w_exit("$DBI::errstr");
    &w_info(sprintf("Summary -> HTTPReqs = %s, NumPosts = %s, NumUsers = %s, PhotoComments = %s, PhotoLikes = %s, VideoComments = %s, VideoLikes = %s, GMedia = %s, GFollows = %s, GFollowedBy = %s", $num_http_reqs, $metrics->{posts}, $metrics->{users}->{global}->{new} + $metrics->{users}->{global}->{old}, $metrics->{image_comments}, $metrics->{image_likes}, $metrics->{video_comments}, $metrics->{video_likes}, $glob_posts, $glob_follows, $glob_followed_by), $profile_id, $profile_cat) if $show_stats;
}

sub addToEntityFan {
    my $self         = shift;
    my $post_link    = shift;
    my $profile_id   = shift;
    my $username     = shift;
    my $userid       = shift;
    my $rel          = shift;
    my $status       = shift;
    my $message      = shift;
    my $commentid    = shift;
    my $created_time = shift; # This is seconds since epoch
    my $profile_cat  = shift;

    $commentid = 0 if $commentid eq '';

    my $sql = sprintf "INSERT INTO ".$self->getTableName('entity_fan')." SET post_link = %s, profile_id = %s, username = hex(AES_ENCRYPT(%s, unhex(\@aa_key))), userid = hex(AES_ENCRYPT(%s, unhex(\@aa_key))), fan_relationship = %s, status = %s, message = %s, comment_id = %s, created_time = %s, fetch_time = now()", $self->{_dbh}->quote($post_link), $self->{_dbh}->quote($profile_id), $self->{_dbh}->quote($username), $self->{_dbh}->quote($userid), $self->{_dbh}->quote($rel), $self->{_dbh}->quote($status), $self->{_dbh}->quote($message), $self->{_dbh}->quote($commentid), $self->{_dbh}->quote($created_time);
    &w_info($sql, $profile_id) if $show_sql;
    if (!$self->{_dbh}->do($sql)) {
        &w_error($sql, $profile_id, $profile_cat);
    }
}


sub updateCrawledPostInEntityFan {
    my $self       = shift;
    my $post_link  = shift;
    my $profile_id = shift;

    my $sql = sprintf "UPDATE ".$self->getTableName('entity_fan')." SET status = %s where post_link = %s and profile_id = %s", $self->{_dbh}->quote(STATUS_CRAWLED), $self->{_dbh}->quote($post_link), $self->{_dbh}->quote($profile_id);
    &w_info($sql, $profile_id) if $show_sql;
    $self->{_dbh}->do($sql) || &w_exit("$DBI::errstr");
}

1;
