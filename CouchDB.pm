package CouchDB;

use strict;
use warnings;
use utf8;
use LWP::UserAgent;

sub new {
  my ($class, $host, $port, $options) = @_;

  my $ua = LWP::UserAgent->new;
  $ua->timeout(10);
  $ua->env_proxy;

  return bless {
                ua       => $ua,
                host     => $host,
                port     => $port,
                base_uri => "http://$host:$port/",
               }, $class;
}

sub ua { shift->{ua} }
sub base { shift->{base_uri} }

sub request {
  my ($self, $method, $uri, $content) = @_;

  my $full_uri = $self->base . $uri;
  my $req;

  if (defined $content) {
    $req = HTTP::Request->new( $method, $full_uri, undef, $content );
    $req->header('Content-Type' => 'application/json');
  } else {
    $req = HTTP::Request->new( $method, $full_uri );
  }

  my $response = $self->ua->request($req);

  if ($response->is_success) {
    return {
        status  => "200",
        content => $response->content
    }
  } else {
    return {
        status  => $response->status_line,
        content => $response->content
    };
  }
}

sub delete {
  my ($self, $url) = @_;

  return $self->request(DELETE => $url);
}

sub get {
  my ($self, $url) = @_;

  return $self->request(GET => $url);
}

sub put {
  my ($self, $url, $json) = @_;

  return $self->request(PUT => $url, $json);
}

sub post {
  my ($self, $url, $json) = @_;

  return $self->request(POST => $url, $json);
}

1;
