#!/usr/bin/env perl -wl

use strict;
use warnings;
use utf8;
use JSON::Tiny qw(decode_json encode_json);
use CouchDB;
use Data::Dumper;
use InstagramNet;
use WebstagramLog;
use DBI;
use Sys::Hostname;

binmode(STDIN,  ':encoding(UTF-8)');
binmode(STDOUT, ':encoding(UTF-8)');
binmode(STDERR, ':encoding(UTF-8)');

my $doc_id = shift;
my $sleep  = 5;

if (not defined $doc_id) {
    print "syntax: $0 <file>";
    exit 0;
}

sub extract_json($) {
    my $data = shift;
    my @lines = split(/\n/, $data);
    my $json;

    foreach (@lines) {
        chomp;
        if ($_ =~ /window._sharedData/) {
            $json = $_;
        }
    }

    if (defined $json) {
        &w_debug("Grabbed the primary JSON", "global");
        $json =~ s/^.*window._sharedData\s*=\s*//;
        $json =~ s/};<\/script>/}/;
    } else {
        &w_error("FATAL: Unable to extract JSON from page. Investigate and check the page structure.", "global");
    }
    
    return $json;
}

sub print_profile_info($) {
    my $json = shift;
    my $ptr  = $json->{entry_data}->{UserProfile}->[0]->{user};
    #&w_info(sprintf("Summary :: media = %d, follows = %d, followed-by = %d", $ptr->{counts}->{media}, $ptr->{counts}->{follows}, $ptr->{counts}->{followed_by}), $json->{_id});
}

my $dbname = "instagram_com";
my $handle = CouchDB->new("localhost", 5984);
my $insta  = InstagramNet->new("instagram.com", 80);
my $dbh    = DBI->connect('DBI:mysql:instagram_crawl;host=localhost', 'root', '', {RaiseError => 1});

sub process_next_profile {
    my $sth    = $dbh->prepare("select * from instagram_crawl.job_entities where status = 'new' limit 1");
    $sth->execute() || die $!;
    my $res    = $sth->fetchrow_hashref();
    my $my_hostname = hostname;
    $my_hostname =~ s/.colligent.com$|.affinityanswers.com$//g;

    if ($res) {
        # Set the profile that needs to be crawled
        $doc_id      = $res->{profile_id};
        &w_info(">>-> Working on $doc_id", "global");
        my $sql = sprintf "UPDATE instagram_crawl.job_entities set status = 'crawling', crawl_start = now(), hostname = %s where profile_id = %s and status = 'new'", $dbh->quote($my_hostname),$dbh->quote($res->{profile_id}); 
        &w_info($sql, $doc_id);
        $dbh->do($sql) || die $!;
        # 1. Download the primary page
        my $doc      = $insta->get($doc_id, "http://instagram.com/");
        my $raw_json = extract_json($doc);
        my $json     = decode_json($raw_json);

        &w_info("CSRF Token : $json->{config}->{csrf_token}", $doc_id);

        #my $resp = $handle->delete("$dbname/$doc_id");
        #if ($resp->{status} =~ /4\d\d/i) {
        #    my $srv_doc_raw  = $handle->get("$dbname/$doc_id");
        #    if ($srv_doc_raw->{status} eq "200") {
        #        my $srv_doc_json = decode_json($srv_doc_raw->{content});
        #        $json->{_id}  = $doc_id;
        #        $json->{_rev} = $srv_doc_json->{_rev};
        #        print "[delete] ID       : ".$json->{_id};
        #        print "[delete] Revision : ".$json->{_rev};
        #        $raw_json = encode_json($json);
        #        $resp = $handle->delete("$dbname/$doc_id?rev=".$json->{_rev});
        #        if ($resp->{status} ne "200") {
        #            print Dumper($resp);
        #            print "ERROR: Failed to delete the document ....";
        #            exit 1;
        #        } else {
        #            print "[delete] completed";
        #        }
        #    }
        #}

        # Set dummy document
        my $resp = $handle->put("$dbname/$doc_id", "{}");

        # Make sure that response contains valid JSON by inspecting the CSRF Token.
        if (defined $json->{config}->{csrf_token}) {
            my $srv_doc_raw  = $handle->get("$dbname/$doc_id");
            if ($srv_doc_raw->{status} eq "200") {
                #print Dumper($srv_doc_raw);
                my $srv_doc_json = decode_json($srv_doc_raw->{content});
                $json->{_id}  = $doc_id;
                $json->{_rev} = $srv_doc_json->{_rev};
                &w_info("ID       : ".$json->{_id});
                &w_info("Revision : ".$json->{_rev});
                $raw_json = encode_json($json);
                $resp = $handle->put("$dbname/$doc_id", $raw_json);
                if ($resp->{status} ne "200") {
                    print Dumper($resp);
                    &w_error("ERROR: Unable to save the document via update. Aborting ..", $doc_id);
                    exit 1;
                }
                &print_profile_info($srv_doc_json);
            } else {
                &w_error("ERROR: Document was deleted. Before it was updated. Status is ".$srv_doc_raw->{status}, $doc_id);
                exit 1;
            }
            # 2. Download all the subsequent documents 
            &w_info("More Available  ? : ".$json->{entry_data}->{UserProfile}->[0]->{moreAvailable}, $doc_id);
            if (defined $json->{entry_data}->{UserProfile}->[0]->{moreAvailable} and $json->{entry_data}->{UserProfile}->[0]->{moreAvailable}) {
                my @maxels      = @{$json->{entry_data}->{UserProfile}->[0]->{userMedia}};
                my $maxel_count = scalar(@maxels);
                my $max_id      = $maxels[$maxel_count - 1]->{id};
                &w_info("Max ID : $max_id", $doc_id);
                while($max_id) {
                    # 1. Make request to local couchdb instance and download the document
                    my $srv_doc_raw = $handle->get("$dbname/$doc_id");
                    if ($srv_doc_raw->{status} eq "200") {
                        # 2. Save the next set of items from the max-id
                        my $more_raw   = $insta->get("$doc_id/media?max_id=$max_id", "http://instagram.com/$doc_id");
                        my $more_json  = decode_json($more_raw);
                        if ($more_json->{status} eq 'ok') {
                            #print Dumper($more_json);
                            my @items = @{$more_json->{items}};
                            my $item_count  = scalar(@items);
                            &w_info("Found $item_count items in $max_id", $doc_id);
                            $max_id = $items[$item_count - 1]->{id};
                            &w_info("Max ID : $max_id", $doc_id);
                            exit 1 if not defined $max_id;
                            my $new_srv_doc = decode_json($srv_doc_raw->{content});
                            push(@{$new_srv_doc->{entry_data}->{UserProfile}->[0]->{userMedia}}, @items);
                            # 3. Save the new document in the server.
                            $raw_json = encode_json($new_srv_doc);
                            $resp = $handle->put("$dbname/$doc_id", $raw_json);
                            if ($resp->{status} ne "200") {
                                #print Dumper($resp);
                                &w_error("ERROR: Unable to save the document via update. Aborting ..", $doc_id);
                                exit 1;
                            } else {
                                &w_debug("------> Added $item_count items to the remote server", $doc_id);
                                $sql = sprintf "UPDATE instagram_crawl.job_entities set num_posts = num_posts + %d where profile_id = %s and status = 'crawling' and hostname = %s", $item_count, $dbh->quote($res->{profile_id}), $dbh->quote($my_hostname);
                                &w_info($sql, $doc_id);
                                $dbh->do($sql) || die $!;
                            }
                            if (!$more_json->{more_available}) {
                                &w_info("Stopping the loop as more_available is false from server.", $doc_id);
                                $max_id = undef;
                                # FIXME: Set the more_available to false here
                                #my $fin_doc_raw  = $handle->get("$dbname/$doc_id");
                                #if ($fin_doc_raw->{status} eq "200") {
                                #    my $fin_doc_json = decode_json($fin_doc_raw->{content});
                                #    if (ref $fin_doc_json->{entry_data}->{UserProfile}->[0]->{moreAvailable} eq 'JSON::XS::Boolean') {
                                #        $fin_doc_json->{entry_data}->{UserProfile}->[0]->{moreAvailable} = 'false';
                                #    }
                                #} else {
                                #    print "Failed setting the more_available status ..";
                                #}
                                $sql = sprintf "UPDATE instagram_crawl.job_entities set status = 'crawled', crawl_end = now() where profile_id = %s and status = 'crawling' and hostname = %s", $dbh->quote($res->{profile_id}), $dbh->quote($my_hostname);
                                &w_info($sql, $doc_id);
                                $dbh->do($sql) || die $!;
                            }
                        } else {
                            $max_id = undef;
                            $sql = sprintf "UPDATE instagram_crawl.job_entities set status = 'crawled', crawl_end = now() where profile_id = %s and status = 'crawling' and hostname = %s", $dbh->quote($res->{profile_id}), $dbh->quote($my_hostname);
                            &w_info($sql, $doc_id);
                            $dbh->do($sql);
                            &w_info("Stopping the loop as the next maxid is not available.", $doc_id);
                            exit 0;
                        }
                        sleep(5);
                    } else {
                        &w_info("No need to download the comments for the parent document as it was deleted !!", $doc_id);
                        exit 1;
                    }
                }
            }
            &w_info("Completed building the full document ....", $doc_id);
            my $analyse_rels = 0;
            if ($analyse_rels) {
                &w_info("Analysing the document for proper relationships", $doc_id);
                my $srv_doc_raw  = $handle->get("$dbname/$doc_id");
                if ($srv_doc_raw->{status} eq "200") {
                    my $json = decode_json($srv_doc_raw->{content});
                    # 1. Update these details in the primary user profile table ?
                    my $user = $json->{entry_data}->{UserProfile}->[0]->{user}->{counts};
                    my ($a, $b, $c, $d, $e, $f, $g, $h) = (
                        $dbh->quote($user->{media}),
                        $dbh->quote($user->{follows}),
                        $dbh->quote($user->{followed_by}),
                        $dbh->quote($doc_id),
                        $dbh->quote($user->{website}),
                        $dbh->quote($user->{full_name}),
                        $dbh->quote($user->{profile_picture}),
                        $dbh->quote($user->{bio}),
                    );
                    $dbh->do("INSERT INTO entity_details set num_media = %d, num_follows = %d, num_followedby = %d, profile_id = %s, website = %s, full_name = %s, profile_picture = %s, bio = %s ON DUPLICATE KEY UPDATE num_media = %d, num_follows = %d, num_followedby = %d, profile_id = %s, website = %s, full_name = %s, profile_picture = %s, bio = %s",
                        $a, $b, $c, $d, $e, $f, $g, $h,
                        $a, $b, $c, $e, $f, $g, $h);
                    # 2. Traverse through all the posts of the profile
                    my @userMedia = @{$json->{entry_data}->{UserProfile}->[0]->{userMedia}};
                    foreach my $post (@userMedia) {
                        my @postComments = @{$post->{comments}->{data}};
                        # Add the comments to the entity_fan table
                        foreach my $comment (@postComments) {
                            my $tbl = "entity_fanXXYYZZ";
                            $sql = sprintf "INSERT INTO $tbl (post_link, post_id, from_id, from_full_name, from_username, text, created_time, comment_id, fan_relationship) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                    $dbh->quote($post->{link}),
                                    $dbh->quote($post->{id}),
                                    $dbh->quote($comment->{from}->{id}),
                                    $dbh->quote($comment->{from}->{full_name}),
                                    $dbh->quote($comment->{from}->{username}),
                                    $dbh->quote($comment->{from}->{text}),
                                    $dbh->quote($comment->{from}->{created_time}),
                                    $dbh->quote($comment->{from}->{id}), # comment_id ?
                                    $dbh->quote('instagram_commenters');
                            $dbh->do($sql) || &w_error($!);
                        }
                        my @postLikes = @{$post->{likes}->{data}};
                        # Add the likes to the entity_fan table
                        foreach my $like (@postLikes) {
                            my $tbl = "entity_fanXXYYZZ";
                            # FIXME: Now, here is a problem. For the likes we do not have the user-id
                            # So, if the user is already seen by our system. the merge process will work fine
                            # Else, merge process has to rely on the username field instead of the id field. because
                            # the id field is not available for the likers !!!
                            # MAJOR BLOCKER MAJOR BLOCKER
                            #$sql = sprintf "INSERT INTO $tbl (post_link, post_id, from_id, from_full_name, from_username, text, created_time, id, fan_relationship) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                            #        $dbh->quote($post->{link}),
                            #        $dbh->quote($post->{id}),
                            #        $dbh->quote('null'),
                            #        $dbh->quote($comment->{from}->{full_name}),
                            #        $dbh->quote($comment->{from}->{username}),
                            #        $dbh->quote(''),
                            #        $dbh->quote(0),
                            #        $dbh->quote($comment->{from}->{id}), # Like id ?
                            #        $dbh->quote('instagram_likers');
                            #$dbh->do($sql) || &w_error($!);
                        }

                    }
                } else {
                    &w_error("Failed to analyse the document for proper relationships", $doc_id);
                    # FIXME: What state should we put these profiles in ?
                }
            }
        }
        return 1;
    }
    $sth->finish();
    return 0;
}

while(&process_next_profile()) {
    &w_info("Sleeping for $sleep seconds before continuing...", "global");
    sleep(5);
}

$dbh->disconnect();

exit 0;
