#!/bin/bash

=cut
MariaDB [instagram_crawl]> create table relationship_map (id int unsigned, rel varchar(32) not null);
Query OK, 0 rows affected (0.39 sec)

MariaDB [instagram_crawl]> insert into relationship_map (id, rel) values (1, 'instagram_commenters'), (2, 'instagram_likers');
Query OK, 2 rows affected (0.05 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [instagram_crawl]> insert into relationship_map (id, rel) values (4, 'instagram_video_commenters'), (8, 'instagram_video_likers');
Query OK, 2 rows affected (0.05 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [instagram_crawl]> 

=cut

