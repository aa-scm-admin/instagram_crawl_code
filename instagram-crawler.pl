#!/usr/bin/env perl -wl

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Main Crawler program
# Date: May/2014

use strict;
use warnings;
use utf8;
use Data::Dumper;
use File::Slurp qw(read_file);
use JSON;
use Time::HiRes qw(sleep usleep nanosleep);
use Instagram;
use InstagramCrawlDB;
use InstagramLog;
use InstagramTime;
use InstagramConstants;
use InstagramExtendedRelationships;
use Cwd;
use File::Spec;
use POSIX ();
use FindBin ();
use File::Basename ();
use File::Spec::Functions;

# make the daemon cross-platform, so exec always calls the script
# # itself with the right path, no matter how the script was invoked.
my $script = File::Basename::basename($0);
my $SELF = catfile($FindBin::Bin, $script);

# POSIX unmasks the sigprocmask properly
$SIG{HUP} = sub {
    &w_info("got SIGHUP . restarting ....");
    my @tmpargv = @ARGV;
    unshift($SELF, @tmpargv);
    exec('perl', @tmpargv) || die "$0: couldn't restart: $!";
};

#$ENV{DEBUG} = 1;
#$ENV{VERBOSE} = 1;

my $in_category  = $ARGV[1];
my $debug        = defined $ENV{DEBUG} ? 1 : 0;
my $verbose      = defined $ENV{VERBOSE} ? 1 : 0;
my $cwd          = getcwd;
my $mypath       = $FindBin::Bin;
my $stopper_file = "$mypath/stopper";

binmode(STDIN,  ':encoding(UTF-8)');
binmode(STDOUT, ':encoding(UTF-8)');
binmode(STDERR, ':encoding(UTF-8)');

my $death_profiles = 0;

=cut
mysql> create database instagram_universe;
Query OK, 1 row affected (0.03 sec)

mysql> create database instagram_crawl;
Query OK, 1 row affected (0.03 sec)

mysql> grant all privileges on instagram_crawl.* to webstagram@localhost identified by 'webstagrampass';
Query OK, 0 rows affected (0.13 sec)

mysql> grant all privileges on instagram_universe.* to webstagram@localhost identified by 'webstagrampass';
Query OK, 0 rows affected (0.13 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.30 sec)

mysql>
=cut

my @queue    = ();
my $config   = {};

sub read_config {
    my $path = shift;
    my $c;
    eval {
        my $d = read_file($path);
        $d    =~ s/\/\*.*?\*\///g;
        $c    = decode_json($d);
        if (not defined $c->{version}) {
            &w_exit(".version does not exist in $path Aborting");
        }
        $ENV{SLEEP_SECS_PROFILE} = $c->{crawl_delay}->{profile};
        $ENV{SLEEP_SECS_POSTS}   = $c->{crawl_delay}->{posts};
        $ENV{LOG_FORMAT}         = $c->{log_format};
    };
    if ($@) {
        print $@;
        exit 1;
    }
    return $c
}

$config = &read_config("$mypath/Instagram.json");

sub process_profile {
    my $wdb              = shift;
    my $profile          = shift;
    my $profile_id       = $profile->[0];
    my $profile_cat      = $profile->[1];
    my $profile_age_secs = int($profile->[2]);
    my $max_post_age     = int($profile->[3]);
    my $crawl_limit_str  = convert_time($max_post_age);
    my $extended_rels    = InstagramExtendedRelationships->new($config, $wdb->getDBH());
    my $metrics = {
        stop_crawl_now => 0,
        posts => 0,
        num_http_reqs => 0,
        users => {
            global => {
                new => 0,
                old => 0,
            },
            cur_post => {
                new => 0,
                old => 0,
            }
        },
        image_comments => 0,
        image_likes => 0,
        video_comments => 0,
        video_likes => 0,
        crawl_code => '200 OK'
    };

    if ($config->{crawl_delay}->{profile} > 0) {
        #&w_info("Sleeping for ".$config->{crawl_delay}->{profile}." seconds before fetching all posts for profile.", $profile_id, $profile_cat);
        sleep($config->{crawl_delay}->{profile});
    }

    my $comments_for_profile = 0;
    my $likes_for_profile    = 0;
    my $instacrawl           = Instagram->new(
        $config,
        $profile_id,
        $profile_cat,
        $config->{deep_crawl} eq "true" ? 1 : 0,
        $config->{max_deep_crawl_levels}
    );
    my $posts = $instacrawl->get_frontpage_posts();
    my $post_count = scalar grep { defined $_ } @$posts;
    if ($post_count == 0 ) # This can be due to Private Profile or No Posts in public profile OR 404 Error.
    {
        &w_info((sprintf "NO posts in the frontpage "), $profile_id, $profile_cat);
        if ($instacrawl->get_last_code() ne '200 OK') { # 404 error case
            $metrics->{crawl_code} = $instacrawl->get_last_code();
        }

        return $metrics;
    }
    my $user_stats = $instacrawl->get_user_stats();
    #&w_info((sprintf "Found %d posts in the frontpage (Total Posts = %d, Total Followers = %d, Total Following = %d)", scalar(@$posts), $user_stats->{counts}->{media}, $user_stats->{counts}->{follows}, $user_stats->{counts}->{followed_by}), $profile_id, $profile_cat);

    # Increment the initial # requests to website
    $metrics->{num_http_reqs}++;

    # abort if crawl is not successful.
    if ($instacrawl->get_last_code() ne '200 OK') {
        $metrics->{crawl_code} = $instacrawl->get_last_code();
        return $metrics;
    }

    # Fetch the subsequent posts and process them
    my $can_abort = 0;
    do {
        my $postidx = 0;
        my $postcnt = scalar(@$posts);
        foreach my $post (@$posts) {
            $postidx++;
            &w_info("Processing the Post :  $post->{node}->{shortcode}  ", $profile_id, $profile_cat);
            my $comments_for_post = 0;
            my $likes_for_post = 0;
            my ($post_aaid);

            if ($wdb->{_workmode} ne 'LEGACY') {
                $post_aaid = $wdb->addPost($profile_id, $post->{id}, $post->{link}, $post->{caption}->{text});
            }

            if ($config->{crawl_delay}->{comments} > 0) {
                sleep($config->{crawl_delay}->{comments});
            }

            # Log the post time details
            #my $diff_secs     = time - int($post->{created_time});
            my $diff_secs     = int($post->{node}->{taken_at_timestamp});
            my $diff_secs_str = convert_time($diff_secs);

            # Set a flag for aborting here itself
            #if ($max_post_age > 0 && $diff_secs > $max_post_age) {
                #if ($metrics->{num_http_reqs} == 1 and $config->{always_save_posts_from_frontpage}) {
                    #$can_abort = 1;
                    #&w_info("Frontpage of the profile has posts older than $crawl_limit_str and We are on Frontpage. Continue collecting data anyway ...", $profile_id, $profile_cat) if $postidx <= 1;
                #} else {
                    #$can_abort = 1;
                    #&w_info("Not crawling the profile as we have reached the pre-defined limit of $crawl_limit_str", $profile_id, $profile_cat);
                    #last;
                #}
            #}

            my $post_type = $instacrawl->get_post_type ($post);

            my $post_link = $instacrawl->get_post_link ($post);
            my $post_caption = $instacrawl->get_post_caption ($post);
            if ($post_caption) {
                $post_caption =~ s/^\s+//;#remove the leading white spaces          
                $post_caption =~ s/\s+$//;#remove the training white spaces
                $post_caption =~ s{\n}{ }g;#replace newline with space
                my $post_caption_length = length $post_caption;
                if ( $post_caption_length ge 1500 ) {
                    $post_caption = substr( $post_caption, 0, 1000 );
                    print "Found Long Data and it is truncated \n";
                }
                print "LENGTH[$post_caption_length] : COMMENT[$post_caption] \n" ;
                $post_aaid = $wdb->addPost($profile_id, $post->{id}, $post_link, $post_caption ,$post->{node}->{taken_at_timestamp});
            }

            my ($comments, $likes) = $instacrawl->get_post_comments_likes($post);
            

            # Print the profile so that we know it is being processed
            if ($max_post_age == 0) {
                $crawl_limit_str = "∞ secs";
            }
            &w_info(
                "Post Count : [$postidx/$postcnt] ".$post_type.' -> '.$post_link." comments = ".scalar(@$comments)." likes = ".scalar(@$likes),
                $profile_id,
                $profile_cat
            );

            # Update the commenters in the database
            foreach my $comment (@$comments) {
                # Add the user to fan_universe no matter what
                my ($user_status, $user_aaid);
                #$user_status = $wdb->addUserNew($profile_id, $comment->{node}->{owner});
                if ($wdb->{_workmode} ne 'LEGACY') {
                    $user_aaid   = $user_status->[1];
                    # FIXME: Take the $comment->{id} and save it as unique comment_id in the system.
                    $wdb->addCommentToPost($profile_id, $user_aaid, $post_aaid, $comment);
                    # Update the metrics
                    $metrics->{users}->{cur_post}->{old}++ if ($user_status->[0] == 0);
                    $metrics->{users}->{cur_post}->{new}++ if ($user_status->[0] != 0);
                } else {
                    my $relationship = 'instagram_commenters';
                    $relationship    = 'instagram_video_commenters' if $post_type eq 'video';
                    #&w_info("User Name:".$comment->{node}->{owner}->{username},$profile_id,$profile_cat);
                    #$wdb->addToEntityFan($post->{link}, $profile_id, $comment->{from}->{username}, $comment->{from}->{id}, $relationship, STATUS_NEW, $comment->{text}, $comment->{id}, $comment->{created_time});
                    $wdb->addToEntityFan($post_link, $profile_id, $comment->{node}->{owner}->{username}, $comment->{node}->{owner}->{id}, $relationship, STATUS_NEW, $comment->{node}->{text}, $comment->{node}->{id}, $comment->{node}->{created_at});
                    $metrics->{users}->{cur_post}->{new}++;
                }
                $metrics->{$post_type.'_comments'}++;
                $comments_for_post++;
                # Save this comment for extended relationship lookup
                my %map = (
                                type           => $post_type,
                                scope          => 'commenter',
                                profile_id     => $profile_id,
                                # user_id        => $comment->{user}->{id},
                                user_id        => $comment->{node}->{owner}->{id},
                                post_id        => $post->{node}->{id},
                                #action_id      => $comment->{id},
                                action_id      => $comment->{node}->{id},
                                #time_of_action => $comment->{created_at}
                                time_of_action => $comment->{node}->{created_at}
                            );
                $extended_rels->store(\%map);
            }

            # Update the likers in the database
            foreach my $like (@$likes) {
                # FIXME: Debate between like-id & user-id here
                # Like time is unknown
                my $relationship = 'instagram_likers';
                $relationship    = 'instagram_video_likers' if $post_type eq 'video';
                # For likers, we take the avg(post_time, now)
                my $like_time = int(int($post->{node}->{taken_at_timestamp}) );
                #$wdb->addToEntityFan($post->{link}, $profile_id, $like->{username}, $like->{id}, $relationship, STATUS_NEW, '', '', $like_time, $profile_cat);
                $wdb->addToEntityFan($post_link, $profile_id, $like->{node}->{username}, $like->{node}->{id}, $relationship, STATUS_NEW, '', '', $like_time, $profile_cat);
                $metrics->{$post_type.'_likes'}++;
                $likes_for_post++;
                #my %map = (
                    #type           => $post->{type},
                    #scope          => 'liker',
                    #profile_id     => $profile_id,
                    #user_id        => $like->{id},
                    #post_id        => $post->{id},
                    #action_id      => 'XX',
                    #time_of_action => $like_time
                #);
                my %map = (
                    type           => $post_type,
                    scope          => 'liker',
                    profile_id     => $profile_id,
                    user_id        => $like->{node}->{id},
                    post_id        => $post->{user}->{id},
                    action_id      => 'XX',
                    time_of_action => $like_time
                    );
                $extended_rels->store(\%map);
            }

            if ($wdb->{_workmode} eq 'LEGACY') {
                $wdb->updateCrawledPostInEntityFan($post->{link}, $profile_id);
            }

            $metrics->{users}->{global}->{new} += $metrics->{users}->{cur_post}->{new};
            $metrics->{users}->{global}->{old} += $metrics->{users}->{cur_post}->{old};

            $metrics->{posts}++;

            &w_info(sprintf("Summary : Posts           : Cur = %5d, Total = %5d", $metrics->{posts}, $posts), $profile_id, $profile_cat) if $config->{log}->{show_statistics} eq "yes";

            my ($a, $b, $c);
            $a = $metrics->{users}->{cur_post}->{new};
            $b = $metrics->{users}->{cur_post}->{old};
            $c = $a + $b;
            &w_info(sprintf("Summary : Users [Current] : New = %5d, Old = %5d, Total = %5d", $a, $b, $c), $profile_id, $profile_cat) if $config->{log}->{show_statistics} eq "yes";

            $a = $metrics->{users}->{global}->{new};
            $b = $metrics->{users}->{global}->{old};
            $c = $a + $b;
            &w_info(sprintf("Summary : Users [Global]  : New = %5d, Old = %5d, Total = %5d", $a, $b, $c), $profile_id, $profile_cat) if $config->{log}->{show_statistics} eq "yes";

            # Save the statistics in the database after every post.
            my $user_stats = $instacrawl->get_user_stats();
            $wdb->updateStatistics($profile_id, $profile_cat, $metrics, $user_stats);

            # Print warning in case we are not getting any comments
            if ($comments_for_post == 0) {
                &w_warn($post_link." doesnt have any comments. Need manual verification. ", $profile_id, $profile_cat);
            }
            $comments_for_profile += $comments_for_post;

            $metrics->{users}->{cur_post}->{new} = 0;
            $metrics->{users}->{cur_post}->{old} = 0;
        }

        if ($config->{crawl_delay}->{posts} > 0) {
            #&w_debug("Sleeping for ".$config->{crawl_delay}->{posts}." seconds before fetching next set of posts.", $profile_id, $profile_cat);
            sleep($config->{crawl_delay}->{posts});
        }

        # Save the statistics in the database after set of posts
        my $user_stats = $instacrawl->get_user_stats();
        $wdb->updateStatistics($profile_id, $profile_cat, $metrics, $user_stats, 1);

        # Fetch the next set of posts
        if ($can_abort == 0) {
            # ->{num_http_reqs} are incremented in this code.
            #$posts  = $instacrawl->get_next_posts($metrics);
            $posts  = [];
        } else {
            $posts  = [];
        }
    } while($can_abort == 0 && scalar(@$posts) > 0);

    # In case the current profile did not get any comments. Exit ?
    #if ($comments_for_profile == 0) {
    #    &w_warn("Found 0 comments for $profile_id in the current request.", $profile_id, $profile_cat);
    #    $death_profiles++;
    #}

    #if ($death_profiles >= $config->{death}->{when}->{profiles_with_zero_comments}) {
    #    &w_error("Reached the limit for dead profiles ($death_profiles). Scraping would've been detected or we are unlucky.", $profile_id, $profile_cat);
    #    $metrics->{stop_crawl_now} = 1;
    #}

    $extended_rels->commit($profile_id, $profile_cat);

    return $metrics;
}

# Main program starts here
my $total_processed = 0;
if (-e $stopper_file) {
    # Exit early if stopper file is found.
    &w_info("Found stopper file $stopper_file. Exiting ....");
} else {
    # Continue normal processing
    my $wdb = InstagramCrawlDB->new($config);
    $wdb->setup();
    while (my @profile = $wdb->fetchProfileToCrawl($in_category)) {
        &w_exit("No more profiles found.") if scalar(@profile) != 4;
        my $metrics = &process_profile($wdb, \@profile);
        $wdb->markProfileCrawled(\@profile, $metrics);
        $total_processed++;
        if (-e $stopper_file) {
            &w_info("Found stopper file $stopper_file. Exiting ....");
            last;
        }
        if ($metrics->{stop_crawl_now}) {
            &w_info("Aborting the program now as a flag is set in the program.");
            last;
        }
        # Read the config file for any changes
        $config = &read_config("$mypath/Instagram.json");
    }
}

&w_info("Processed $total_processed profiles from database.", 'SUMMARY', 'SUM');

exit(0);
