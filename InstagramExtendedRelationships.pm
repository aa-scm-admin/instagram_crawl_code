package InstagramExtendedRelationships;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Additional relationship capture from given relationships
# Date: Aug/2014

use utf8;
use strict;
use warnings;
use InstagramNet;
use InstagramLog;
use InstagramRelationships;
use Data::Dumper;
use Redis;
use JSON;

sub new {
    my ($class, $config, $dbh) = @_;
    my $cache;
    my $relmap;
    if ($config->{enable_extended_relationships} eq 'yes') {
        $cache  = Redis->new(server => $config->{cache_hostname});
        $relmap = InstagramRelationships->new($config, $dbh);
    }
    return bless {
        config           => $config,
        max_allowed_time => $config->{activity_filter_seconds},
        dbh              => $dbh,
        cache            => $cache,
        relmap           => $relmap
    }, $class;
}

=cut
    Generate key for any one of these types

    - content_liker                           : liked one post
        - video_content_liker                 : liked one *video* post
        - photo_content_liker                 : liked one *photo* post
    - frequent_content_liker                  : liked more than one post
        - frequent_video_content_liker        : liked more than one *video* post
        - frequent_photo_content_liker        : liked more than one *photo* post
    - content_commenter                       : commented on one post
        - video_content_commenter             : commented on one *video* post
        - photo_content_commenter             : commented on one *photo* post
    - frequent_content_commenter              : commented on more than one post
        - frequent_video_content_commenter    : commented on more than one *video* post
        - frequent_photo_content_commenter    : commented on more than one *photo* post

=cut
sub keymaker {
    my ($self, $kv) = @_;
    my $type        = lc($kv->{type});
    my $scope       = lc($kv->{scope});
    my $profile_id  = $kv->{profile_id};
    my $user_id     = $kv->{user_id};
    my $post_id     = $kv->{post_id};
    my $comment_id  = $kv->{action_id}; # comment or a like
    my $epoch_secs  = $kv->{time_of_action};
    my @keys = ();
    if ($epoch_secs > time - $self->{max_allowed_time}) {
        if ($scope eq 'liker') {
            # Content Liker
            #push(@keys, "$profile_id|CL_ALL|$user_id|$post_id");
            # Content Liker - Video
            #push(@keys, "$profile_id|CL_VID|$user_id|$post_id") if $type eq 'video';
            # Content Liker - Photo
            #push(@keys, "$profile_id|CL_PIC|$user_id|$post_id") if $type eq 'photo';
            # Frequent Content Liker
            push(@keys, "$profile_id|FCL_ALL|$user_id");
            # Frequent Content Liker - Video
            #push(@keys, "$profile_id|FCL_VID|$user_id") if $type eq 'video';
            # Frequent Content Liker - Photo
            #push(@keys, "$profile_id|FCL_PIC|$user_id") if $type eq 'photo';
        } else {
            # Content Commenter
            #push(@keys, "$profile_id|CC_ALL|$user_id|$post_id");
            # Content Commenter - Video
            #push(@keys, "$profile_id|CC_VID|$user_id|$post_id") if $type eq 'video';
            # Content Commenter - Photo
            #push(@keys, "$profile_id|CC_PIC|$user_id|$post_id") if $type eq 'photo';
            # Frequent Content Commenter
            push(@keys, "$profile_id|FCC_ALL|$user_id");
            # Frequent Content Commenter - Video
            #push(@keys, "$profile_id|FCC_VID|$user_id") if $type eq 'video';
            # Frequent Content Commenter - Photo
            #push(@keys, "$profile_id|FCC_PIC|$user_id") if $type eq 'photo';
        }
    }
    return @keys;
}

=cut
    Store the given set of attributes into the Redis cache
    Type of extended relationships supported
=cut
sub store {
    my ($self, $kv) = @_;
    if ($self->{config}->{enable_extended_relationships} eq 'yes') {
        my @keys  = $self->keymaker($kv);
        # save all the keys from keymaker
        foreach my $key (@keys) {
            &w_debug("Saving $key into cache");
            $self->{cache}->incr($key);
        }
    }
}

=cut
    Process all the keys from Redis and commit them to database
=cut
sub commit {
    my ($self, $profile_id, $profile_cat) = @_;
    my ($sql, $rel, $val);
    if ($self->{config}->{enable_extended_relationships} eq 'yes') {
        my @patterns = ("$profile_id|FCL_ALL|*", "$profile_id|FCC_ALL|*");
        foreach my $pattern (@patterns) {
            my @keys = $self->{cache}->keys($pattern);
            &w_info(sprintf("Found %d keys inside the cache for pattern '%s'", scalar(@keys), $pattern), $profile_id, $profile_cat);
            for(my $q = 0; $q < scalar(@keys); $q++) {
                my $key   = $keys[$q];
                my @parts = split(/\|/, $key);
                next if $parts[0] ne $profile_id;
                $val = $self->{cache}->get($key);
                $rel = $self->find_relationship($parts[1], $val);
                $sql = sprintf "insert into entity_fan_extended set category = '%s', profile_id = '%s', fan_id = hex(aes_encrypt('%s', unhex(\@aa_key))), fan_relationship = '%s', num_times = %d on duplicate key update num_times = num_times + %d", $profile_cat, $parts[0], $parts[2], $rel, $val, $val;
                my $rc = $self->{dbh}->do($sql);
                &w_debug($sql ." <- RC : $rc");
                $self->{cache}->del($key);
            }
        }
    }
}

sub find_relationship {
    my ($self, $lookup, $val) = @_;
    my $value;
    if ($lookup eq 'FCL_ALL') {
        if ($val <= 1) {
            $value = 'instagram_content_liker';
        } else {
            $value = 'instagram_frequent_content_liker';
        }
    } elsif ($lookup eq 'FCC_ALL') {
        if ($val <= 1) {
            $value = 'instagram_content_commenter';
        } else {
            $value = 'instagram_frequent_content_commenter';
        }
    } else {
        $value = 'unknown';
    }
    return $self->{relmap}->rel_fw($value);
}

1;
