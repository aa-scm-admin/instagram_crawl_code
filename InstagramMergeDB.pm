package InstagramMergeDB;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Database interaction layer for Merge
# Date: Jun/2014

use strict;
use warnings;
use utf8;
use DBI;
use Data::Dumper;
use InstagramLog;
use Sys::Hostname;
use InstagramConstants;
#use InstagramRelationships;

my $debug    = defined $ENV{DEBUG} ? 1 : 0;
my $verbose  = defined $ENV{VERBOSE} ? 1 : 0;
my $show_sql = defined $ENV{SHOW_SQL} ? 1 : 0;

sub new {
    my $class = shift;
    my $config= shift;
    my $self  = {
        _dbh        => undef,
        _myname     => hostname,
        rels        => {},
        config      => $config
    };
    print Dumper($self) if $debug;
    $self->{_myname} =~ s/\.(colligent|affinityanswers)\.com//;
    bless($self, $class);    # make $self an object of class $class
    return $self;
}

sub setup() {
    my $self = shift;
    my $cfg  = $self->{config}->{db}->{merge};
    $self->{_dbh} = DBI->connect(
        'DBI:mysql:database='.$cfg->{db}.';host='.$cfg->{host}, $cfg->{user}, $cfg->{pass}
    ) || &w_exit("Error: Could not connect to database: $DBI::errstr");

    # Set the sql strict mode
    $self->{_dbh}->do("set sql_mode='STRICT_ALL_TABLES'") || &w_exit("Error: Failed to set strict mode.");

    # Disable query cache for this session
    $self->{_dbh}->do("SET SESSION query_cache_type = OFF") || &w_exit("Error: Failed to turn-off query cache.");

    # create the session variable to store the key
    # Without this variable, all values encrypted will be just NULLs
    $self->{_dbh}->do("set \@aa_key='".$self->{config}->{hex_key}."';") || &w_exit("Error: Failed to set key.");

    #$self->{relobj} = new InstagramRelationships($self->{config}, $self->{_dbh});

    # Fetch all the relationships from the database
    my $tbl = $self->getTableName("relationship_map");
    my $sql = "SELECT id, rel from $tbl";
    my $sth = $self->{_dbh}->prepare($sql);
    my $rc  = 0;
    $sth->execute || die $!;
    while (my $row = $sth->fetchrow_hashref) {
        $self->{rels}->{forward}->{$row->{rel}} = $row->{id};
        $self->{rels}->{reverse}->{'r'.$row->{id}}  = $row->{rel};
        $rc++;
    }
    my $xi = $self->{config}->{death}->{when}->{min_relationship_maps};
    if (not defined $xi or $rc < $xi) {
        &w_error("Aborting as there are not enough (required = $xi, found = $rc) relationships found in $tbl table");
        exit 1;
    }
}

sub rel_fw {
    my ($self, $name) = @_;
    return $self->{rels}->{forward}->{$name} if defined $self->{rels}->{forward}->{$name};
    &w_error("Programming error. $name is not a valid relationship as per database. Please correct it before continuing.");
    exit 1;
    #return $self->{relobj}->rel_fw($name);
}
sub rel_rr {
    my ($self, $num) = @_;
    return $self->{rels}->{reverse}->{'r'.$num} if defined $self->{rels}->{forward}->{'r'.$num};
    &w_error("Programming error. $num is not a valid relationship-value as per database. Please correct it before continuing.");
    exit 1;
    #return $self->{relobj}->rel_rr($num);
}

sub getTableName($) {
    my $self  = shift;
    my $name  = shift;
    my $table = $self->{config}->{tables}->{$name};
    if (not defined $table) {
        &w_exit("'$name' is not found in Instagram.json. Aborting");
    }
    return $table;
}

sub addUserToUUID($$$) {
    my $self = shift;
    my $profile_id = shift;
    my $from = shift;

    # Fetch the user's AUTOINCREMENTED id from Encrypted Fan-ID
    my $sql = sprintf "SELECT uuid from ".$self->getTableName('UUID_ig')." where fan_id = '%s'", $from->{id};
    my $sth = $self->{_dbh}->prepare($sql);
    if (!$sth) {
        &w_exit($sql);
    }
    if (!$sth->execute()) {
        &w_exit($sql);
    }
    my @result = $sth->fetchrow_array();
    my $return = [];
    my ($a, $b);
    if (defined $result[0]) {
        # User already exists in the system
        $return = [0, $result[0]];
        # To be inline with Facebook & Twitter use the time as the one on when its merged
        # rather than the one we got from network
        if ($self->{config}->{merge}->{last_updt_time} eq 'from_network') {
            $sql = sprintf "UPDATE ".$self->getTableName('UUID_ig')." SET last_updt_time = FROM_UNIXTIME(GREATEST(%s, UNIX_TIMESTAMP(last_updt_time))) where uuid = %s", $from->{created_time}, $result[0];
        } else {
            $sql = sprintf "UPDATE ".$self->getTableName('UUID_ig')." SET last_updt_time = now() where uuid = %s", $result[0];
        }
        $a = $self->{_dbh}->do($sql);
        if (!$a) {
            print Dumper($return);
            print Dumper($from);
            &w_exit($sql." <-> ".$DBI::errstr);
        }
    } else {
        # Insert the user into the system (this is already encrypted in entity_fan)
        # To be inline with Facebook & Twitter use the time as the one on when its merged
        # rather than the one we got from network
        if ($self->{config}->{merge}->{last_updt_time} eq 'from_network') {
            $sql = sprintf "INSERT INTO ".$self->getTableName('UUID_ig')." (fan_id, last_updt_time) VALUES ('%s', FROM_UNIXTIME(%s))", $from->{id}, $from->{created_time};
        } else {
            $sql = sprintf "INSERT INTO ".$self->getTableName('UUID_ig')." (fan_id, last_updt_time) VALUES ('%s', now())", $from->{id};
        }
        $a = $self->{_dbh}->do($sql);
        if (!$a) {
            print Dumper($return);
            print Dumper($from);
            &w_exit($sql." <-> ".$DBI::errstr);
        }
        $b = $self->{_dbh}->{'mysql_insertid'};
        &w_debug("Added user ".$from->{username}." with id ".$b, $profile_id);
        # Return the auto-incremented value to the user
        $return = [1, $b];
    }
    return $return;
}

sub countProfilesToBeMerged {
    my $self = shift;
    my $cat  = shift;
    my $sql  = sprintf "SELECT count(*) as total from ".$self->getTableName('job_entities')." where status = %s", $self->{_dbh}->quote(STATUS_CRAWLED);
    if (defined $cat and length($cat) eq 3) {
        $sql  = sprintf "SELECT count(*) as total from ".$self->getTableName('job_entities')." where status = %s and category = %s", $self->{_dbh}->quote(STATUS_CRAWLED), $self->{_dbh}->quote($cat);
    }
    my $sth = $self->{_dbh}->prepare($sql);
    if (!$sth) {
        &w_exit($sql."; ".$DBI::errstr);
    }
    $sth->execute() || &w_exit($sql." ; ".$DBI::errstr);
    my @result = $sth->fetchrow_array();
    if (defined $result[0]) {
        return $result[0];
    }
    return 0;
}

sub fetchProfileToMerge {
    my $self = shift;
    my $cat  = shift;
    my $sql  = sprintf "SELECT aa_id, profile_id, category from ".$self->getTableName('job_entities')." where status = %s order by aa_id asc LIMIT 1", $self->{_dbh}->quote(STATUS_CRAWLED);
    if (defined $cat and length($cat) eq 3) {
        $sql  = sprintf "SELECT aa_id, profile_id, category from ".$self->getTableName('job_entities')." where status = %s and category = %s order by aa_id asc LIMIT 1", $self->{_dbh}->quote(STATUS_CRAWLED), $self->{_dbh}->quote($cat);
    }
    my $sth = $self->{_dbh}->prepare($sql);
    if (!$sth) {
        &w_exit($sql."; ".$DBI::errstr);
    }
    $sth->execute() || &w_exit($sql.' ;'.$DBI::errstr);
    my @result = $sth->fetchrow_array();
    my $rows   = 0;
    if (defined $result[0]) {
        # Update the status of this profile
        $sql = sprintf "UPDATE ".$self->getTableName('job_entities')." set merge_pid = %d, status = %s, merge_hostname = %s, merge_start_time = now(), merge_end_time = NULL WHERE profile_id = %s", $$, $self->{_dbh}->quote(STATUS_MERGING), $self->{_dbh}->quote($self->{_myname}), $self->{_dbh}->quote($result[1]);
        &w_info($sql, $result[1], $result[2]) if $show_sql;
        $rows = $self->{_dbh}->do($sql) || &w_exit("$sql ; $DBI::errstr");
    }
    return @result;
}

sub markProfileMerged {
    my $self = shift;
    my $profile_id = shift;
    my $ret_map = shift;
    my $profile_cat = shift;
    my $tot_seq = shift;
    my $cur_seq = shift;
    my $count = $ret_map->[0];
    my $new_users = $ret_map->[1];

    # Update the status of this profile
    my $sql = sprintf "UPDATE ".$self->getTableName('job_entities')." set status = %s, merge_end_time = now(), merge_pid = %d WHERE profile_id = %s", $self->{_dbh}->quote(STATUS_MERGED), $$, $self->{_dbh}->quote($profile_id);
    &w_info($sql, $profile_id, $profile_cat) if $show_sql;
    my $rows = $self->{_dbh}->do($sql) || &w_exit("$sql ; $DBI::errstr");

    &w_info("[$cur_seq/$tot_seq] Merge complete. Processed $count relationships, Found $new_users new fans.", $profile_id, $profile_cat);
    &w_info(sprintf("[$cur_seq/$tot_seq] [summary] Inserted  %5d records (fan<->entity relationships) into entity_fan_XXX_master tables.", $ret_map->[2]->{inserted_new_fan_entity_map}), $profile_id, $profile_cat);
    &w_info(sprintf("[$cur_seq/$tot_seq] [summary] Updated   %5d records (relationship bits or timestamp change) in entity_fan_XXX_master tables.", $ret_map->[2]->{updated_entity_relbits}), $profile_id, $profile_cat);
    &w_info(sprintf("[$cur_seq/$tot_seq] [summary] No-Change %5d records from entity_fan haven't made any changes to entity_fan_XXX master tables.", $ret_map->[2]->{no_change}), $profile_id, $profile_cat);
    &w_info(sprintf("[$cur_seq/$tot_seq] [summary] Unknown   %5d records", $ret_map->[2]->{unknown}), $profile_id, $profile_cat);

    # Make sure that checksum is good. Did not loose any bits
    if ($ret_map->[2]->{inserted_new_fan_entity_map}
        + $ret_map->[2]->{updated_entity_relbits}
        + $ret_map->[2]->{no_change}
        + $ret_map->[2]->{unknown} != $count) {
        &w_exit("Total records in the entity_fan does not match with processed records. Aborting");
    }
    return $rows;
}

sub mergeProfile {
    my ($self, $profile_uid, $profile_id, $profile_cat, $merge_date) = @_;
    print $merge_date;
    my $sql  = sprintf "SELECT post_link, profile_id, username, userid, fan_relationship, message, status, comment_id, created_time, FROM_UNIXTIME(fetch_time) as fetch_time from ".$self->getTableName('entity_fan')." where profile_id = %s", $self->{_dbh}->quote($profile_id);
    my $sth = $self->{_dbh}->prepare($sql);
    $sth->execute() || die "$!";
    my $count = 0;
    my $new_users = 0;
    my $retmap = {
        # A new fan <-> entity relationship is added to the table
        inserted_new_fan_entity_map => 0,
        # Updated relationship bits to existing fan <-> entity relationship
        updated_entity_relbits => 0,
        # No change,
        no_change => 0,
        # unknown return value !
        unknown => 0
    };
    while(my $row = $sth->fetchrow_hashref) {
        $count++;
        # 1. Add this encrypted fan to UUID_ig
        my $from = {
            # (Encrypted) Unique number in instagram system
            id => $row->{userid},
            # (Encrypted) Unique username in instagram system
            username => $row->{username},
            # UNIX Timestamp on when the comment was done
            # For instagram_{photo,video}_likes its zero
            created_time => $row->{created_time},
            # UNIX Timestamp(Fetch time) is helpful as placeholder for likers time
            fetch_time => $row->{fetch_time}
        };
        my $user_uuid = $self->addUserToUUID($profile_id, $from);
        if (scalar(@$user_uuid) == 2) {
            # 2. Insert this fan into appropriate bucket with relationship
            my ($rel_hex, $master, $tbl);
            if ($self->{config}->{numeric_relationships_in_entity_fan}) {
                $rel_hex = $row->{fan_relationship};
            } else {
                $rel_hex = $self->relationshipBits($row->{fan_relationship});
            }
            $master  = "entity_fan_".$profile_cat."_master";
            $tbl     = $self->getTableName($master);
            &w_debug("Master = $master, Table = $tbl, rel-hex = $rel_hex", $profile_id, $profile_cat);
            # When a record is inserted $rows will return 1
            # When a record is updated (coz its primary key) $rows will return 2
            # When record is neither inserted/nor updated $rows will return 0
            my $sql_up = "";
            # To be inline with Facebook & Twitter use the time as the one on when its merged
            # rather than the one we got from network
            if ($self->{config}->{merge}->{last_updt_time} eq 'from_network') {
                $sql_up = sprintf("INSERT INTO $tbl set entity_uid = %s, fan_id = '%s', uuid = %s, fan_relationship = %s, last_act_time = GREATEST(%s, last_act_time) ON DUPLICATE KEY UPDATE fan_relationship = fan_relationship | %d, last_act_time = GREATEST(%s, last_act_time)", $profile_uid, $row->{userid}, $user_uuid->[1], $rel_hex, $from->{created_time}, $rel_hex, $from->{created_time});
            } else {
                # $sql_up = sprintf("INSERT INTO $tbl set entity_uid = %s, fan_id = '%s', uuid = %s, fan_relationship = %s, last_act_time = UNIX_TIMESTAMP(now()) ON DUPLICATE KEY UPDATE fan_relationship = fan_relationship | %d, last_act_time = UNIX_TIMESTAMP(now())", $profile_uid, $row->{userid}, $user_uuid->[1], $rel_hex, $rel_hex);
                $sql_up = sprintf("INSERT INTO $tbl set entity_uid = %s, fan_id = '%s', uuid = %s, fan_relationship = %s, last_act_time = UNIX_TIMESTAMP('%s') ON DUPLICATE KEY UPDATE fan_relationship = fan_relationship | %d, last_act_time = UNIX_TIMESTAMP('%s')", $profile_uid, $row->{userid}, $user_uuid->[1], $rel_hex, $merge_date,$rel_hex,$merge_date);
            }
            &w_info($sql_up, $profile_id, $profile_cat) if $show_sql;
            my $rows = $self->{_dbh}->do($sql_up) || die "$sql_up ; $!";
            if ($rows == 0) {
                $retmap->{no_change}++;
            } elsif ($rows == 1) {
                $retmap->{inserted_new_fan_entity_map}++;
            } elsif ($rows == 2) {
                $retmap->{updated_entity_relbits}++;
            } else {
                $retmap->{unknown}++;
            }
            $new_users++ if ($user_uuid->[0] == 1);
        } else {
            &w_exit("Failed to insert record into UUID_ig. Aborting", $profile_id, $profile_cat);
        }
    }
    $sth->finish;
    return [$count, $new_users, $retmap];
}

sub relationshipBits {
    my ($self, $rel) = @_;
    if (defined $rel) {
        return $self->rel_fw($rel);
    }
    die("Invalid $rel. Aborting");
}

1;
