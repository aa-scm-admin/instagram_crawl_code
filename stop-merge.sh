#!/bin/bash

for CAT in `mysql -uinstagram -hstage5 -A instagram -pinstagrampass -e'show tables like "%master%"' | grep master | cut -d_ -f3 | grep -v ^insta`
do
    echo $CAT
    touch $CAT.stopper stopper.$CAT
done
