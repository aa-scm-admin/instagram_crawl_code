#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage : $0 2018may23"
    exit 1
fi

catalog_date="$1"

mysql -uroot -D instagram_crawl -Ne "CREATE TABLE IF NOT EXISTS instagram_crawl.temp_fan_hash_${catalog_date} ( fan_id bigint(20) NOT NULL DEFAULT '0',   fan_id_hash varchar(256) DEFAULT NULL, PRIMARY KEY (fan_id) ) ENGINE=MyISAM;"

mysql -uroot -D instagram_crawl -Ne "INSERT IGNORE INTO temp_fan_hash_${catalog_date} SELECT userid, md5(userid) FROM entity_fan;" && mysql -uroot -D instagram_crawl -Ne "UPDATE entity_fan SET userid_hash = md5(userid) where userid_hash is null and userid is not null;" && mysql -uroot -D instagram_crawl -Ne "UPDATE entity_fan SET userid=0 where userid_hash is not null;"
