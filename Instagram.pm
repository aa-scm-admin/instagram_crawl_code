package Instagram;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: HTTP Scraping Interface For http://stagram.com
# Date: May/2014

use utf8;
use strict;
use warnings;
use InstagramNet;
use InstagramLog;
use Data::Dumper;
use JSON;

sub new {
    my ($class, $config, $profile, $profile_cat, $deep_crawl, $deep_crawl_levels) = @_;
    return bless {
        config                => $config,
        profile               => $profile,
        profile_cat           => $profile_cat,
        can_deep_crawl        => $deep_crawl,
        max_deep_crawl_levels => defined $deep_crawl_levels ? int($deep_crawl_levels) : 2,
        csrf_token            => '',
        cur_level             => 0,
        last_code             => ''
    }, $class;
}

sub extract_json($) {
    my ($self, $data) = @_;
    my @lines = split(/\n/, $data);
    my $json;

    foreach (@lines) {

        chomp;
        if ($_ =~ /window._sharedData/) {
            $json = $_;
            last;
        }
    }

    if (defined $json) {
        &w_debug("Grabbed the primary JSON", $self->{profile}, $self->{profile_cat});
        $json =~ s/^.*window._sharedData\s*=\s*//;
        $json =~ s/};<\/script>/}/;
        #$json =~ s/};<\/script><script type="text\/javascript">/}/;
    } else {
        &w_error("FATAL: Unable to extract JSON from page. Investigate and check the page structure.", $self->{profile}, $self->{profile_cat});
    }

    return $json;
}

sub extract_post_json($) {
    my ($self, $data) = @_;
    my @lines = split(/\n/, $data);
    my $json;
    my $status=1;

    foreach (@lines) {

        chomp;
        if ($_ =~ /window._sharedData/) {
            $json = $_;
            last;
        }
    }

    if (defined $json) {
        &w_debug("Grabbed the primary JSON", $self->{profile}, $self->{profile_cat});
        $json =~ s/^.*window._sharedData\s*=\s*//;
        $json =~ s/};<\/script>/}/;
        #$json =~ s/};<\/script><script type="text\/javascript">/}/;
    } else {
        &w_warn("Unable to extract JSON from post. Investigate and check the post structure.", $self->{profile}, $self->{profile_cat});
        $status = 0;
    }

    return ($json, $status);
}

sub get_last_code() {
    my $self = shift;
    return $self->{last_code} || '';
}

sub print_json {
    my ($json, $filename) = @_;
    open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
    print $fh Dumper($json);
    close $fh;
}


sub get_frontpage_posts() {
    my $self     = shift;
    my $insta    = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);

    my $doc      = $insta->get($self->{profile}, "http://instagram.com/");
    $self->{last_code} = $doc->{status};
    # After a page is found it can be a 404 as well
    if ($doc->{status} ne '200 OK') {
        $self->{last_code} = $doc->{status};
        &w_error("Recieved ".$self->{last_code}." While crawling. Not crawling profile.", $self->{profile}, $self->{profile_cat});
        return [];
    }

    my $raw_json = $self->extract_json($doc->{content});

    eval {
        $self->{frontpage} = decode_json($raw_json);
    };
    if ($@) {
        &w_error("Error occured while processing frontpage. $!", $self->{profile}, $self->{profile_cat});
        return [];
    }

    my $filename = $self->{profile} . "_firstpage.json";
    print_json ($self->{frontpage}, $filename ) ;

    $self->{csrf_token}    = $self->{frontpage}->{config}->{csrf_token};
   
    $self->{cur_posts}     = $self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{graphql}->{user}->{edge_owner_to_timeline_media}->{edges};

    $self->{user_stats}     = $self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{graphql}->{user};
    
    $self->{pages_crawled} = 1;

    return $self->{cur_posts};
}

sub get_new_frontpage_posts() {
    my $self     = shift;
    my $insta    = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);

    my $doc      = $insta->get($self->{profile}, "http://instagram.com/");
    $self->{last_code} = $doc->{status};
    # After a page is found it can be a 404 as well
    if ($doc->{status} ne '200 OK') {
        $self->{last_code} = $doc->{status};
        &w_error("Recieved ".$self->{last_code}." While crawling. Not crawling profile.", $self->{profile}, $self->{profile_cat});
        return [];
    }

    my $raw_json = $self->extract_json($doc->{content});

    eval {
        $self->{frontpage} = decode_json($raw_json);
    };
    if ($@) {
        &w_error("Error occured while processing frontpage. $!", $self->{profile}, $self->{profile_cat});
        return [];
    }

    my $filename = $self->{profile} . "_firstpage.json";
    print_json ($self->{frontpage}, $filename ) ;

    $self->{csrf_token}    = $self->{frontpage}->{config}->{csrf_token};
   
    #$self->{cur_posts}     = $self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{user}->{media}->{nodes};

    $self->{user_stats}     = $self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{user};

    my $profile_user_id = $self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{user}->{id};
    my $url         = "graphql/query/?query_id=17888483320059182&id=".$profile_user_id."&first=1000";
    my $new_doc    = $insta->get($url);
    $self->{last_code} = $new_doc->{status};
    
    if ($new_doc->{status} ne '200 OK') {
        $self->{last_code} = $new_doc->{status};
        &w_error("Recieved ".$self->{last_code}." While getting 1000 posts. Not crawling profile.", $self->{profile}, $self->{profile_cat});
        return [];
    }

    $raw_json = $new_doc->{content};

    eval {
        $self->{frontpage} = decode_json($raw_json);
    };
    if ($@) {
        &w_error("Error occured while processing first 1000 post. $!", $self->{profile}, $self->{profile_cat});
        return [];
    }
    my $new_filename = $self->{profile} . "_first_1000_post.json";
    print_json ($self->{frontpage}, $new_filename ) ;

    $self->{cur_posts}     = $self->{frontpage}->{data}->{user}->{edge_owner_to_timeline_media}->{edges};

    $self->{pages_crawled} = 1;

    return $self->{cur_posts};
}

sub get_csrf_token() {
    my $self = shift;
    return $self->{csrf_token};
}

sub get_posts() {
    my ($self) = @_;
    return $self->{cur_posts};
}

sub get_post_type() {
	my ($self, $post) = @_;
	my $video = $post->{node}->{is_video} ;
	my $type='image';

	if ($video eq 'true')
	{ 
	   $type= 'video';
	}
	return $type;
}

sub get_post_link (){
        my ($self, $post) = @_;

	return "http://instagram.com/p/".$post->{node}->{shortcode};
}
sub get_post_comments_likes() {
    my ($self, $post) = @_;
    # http://instagram.com/empiresandallies
    sleep (2);
    #sleep($config->{crawl_delay}->{posts});
    
    my $code = $post->{node}->{shortcode};
    my @local_comment_array = [];
    my @local_like_array = [];

    my $insta       = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);

    #my $url         = $self->{profile}."/media?max_id=$max_id";
    #my $url         = "/p/".$code."/?taken-by=".$self->{profile};
    #my $url         = "/p/".$code."/";
    my $url         = "p/".$code."/";

    my $doc    = $insta->get($url);

    my ($raw_json,$returnval) = $self->extract_post_json($doc->{content});
    if ($returnval == 0 )
    {
        &w_info("Trying again once for the post  $!", $self->{profile}, $self->{profile_cat});
        sleep (3);
        #sleep($config->{crawl_delay}->{posts});
        $doc    = $insta->get($url);
        ($raw_json,$returnval) = $self->extract_post_json($doc->{content});
        if ($returnval == 0){
             &w_warn("Error occured while processing post. $!", $self->{profile}, $self->{profile_cat});
             return (@local_comment_array,  @local_like_array);
	}
        
    }


    eval {
        $self->{frontpage} = decode_json($raw_json);
    };

    if ($@) {
        &w_warn("Error occured while decoding the json of the post. $!", $self->{profile}, $self->{profile_cat});
        return (@local_comment_array,  @local_like_array);
    }

    my $filename = $self->{profile} . "_".$code.".json";
    print_json ($self->{frontpage}, $filename ) ;

   # if ($self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{comments}->{nodes}) {
	#@local_comment_array = $self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{comments}->{nodes};
   # }
   # if ($self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{likes}->{nodes}) {
	#@local_like_array = $self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{likes}->{nodes};
   # }
    

    if ($self->{frontpage}->{entry_data}->{PostPage}->[0]->{graphql}->{shortcode_media}->{edge_media_to_comment}->{edges}) {
        @local_comment_array = $self->{frontpage}->{entry_data}->{PostPage}->[0]->{graphql}->{shortcode_media}->{edge_media_to_comment}->{edges};
    }
    if ($self->{frontpage}->{entry_data}->{PostPage}->[0]->{graphql}->{shortcode_media}->{edge_media_preview_like}->{edges}) {
        @local_like_array = $self->{frontpage}->{entry_data}->{PostPage}->[0]->{graphql}->{shortcode_media}->{edge_media_preview_like}->{edges};
    }


    return (@local_comment_array,  @local_like_array);

    #return ($self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{comments}->{nodes}, $self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{likes}->{nodes});

}

sub get_comments() {
    my ($self, $post) = @_;
    return $post->{comments}->{data};
}

sub get_likes() {
    my ($self, $post) = @_;
    return $post->{likes}->{data};
}

sub get_user_stats() {
    my ($self) = @_;
    return $self->{user_stats};
}

sub get_post_caption (){
    my ($self, $post) = @_;
    #return $post->{caption};
    return $post->{node}->{edge_media_to_caption}->{edges}->[0]->{node}->{text};

}

sub can_deep_crawl() {
    my ($self) = @_;
    #return $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{moreInitiallyAvailable};
    # return $self->{frontpage}->{entry_data}->{PostPage}->[0]->{media}->{comments}->{page_info}->{has_previous_page};
    return 1;

}

sub get_next_posts_new() {
    my ($self, $metrics) = @_;
    # 1. Find the max post-id
    # 2. Make a http-request. merge the posts
    if ($self->can_deep_crawl && ($self->{max_deep_crawl_levels} > 0 && $self->{cur_level} < $self->{max_deep_crawl_levels})) {
        $metrics->{num_http_reqs}++;
        #my @maxels      = @{$self->{frontpage}->{entry_data}->{ProfilePage}->[0]->{user}->{media}};
        #my $maxel_count = scalar(@maxels);

        my $maxels = $self->{cur_posts};
        my $maxel_count = scalar grep { defined $_ } @$maxels;

        my $max_id = 0;

        #if ($maxel_count > 12 ){
        if ($maxel_count > 11 ){
                $max_id      = @$maxels[$maxel_count - 1]->{id};
        }

        #my $max_id      = $maxels[$maxel_count - 1]->{id};

        my $insta       = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);
        #my $url         = $self->{profile}."/media?max_id=$max_id";
        my $url         = $self->{profile}."/?max_id=$max_id";

        my $more_raw    = $insta->get($url, "http://instagram.com/".$self->{profile});
        my $more_json   = { status => "notok"};
        if ($more_raw->{status} ne '200 OK') {
            &w_error("Got ".$more_raw->{status}." while downloading $url", $self->{profile}, $self->{profile_cat});
            return [];
        }
        my $more_raw_json = $self->extract_json($more_raw->{content});

        eval {
#            $more_json  = decode_json($more_raw->{content});
             $more_json = decode_json($more_raw_json);
 
        };
        if ($@) {
            &w_error("Error occured while processing JSON response from $url : $@", $self->{profile}, $self->{profile_cat});
        }
        #if ($more_json->{status} eq 'ok') {
            #my @items       = @{$more_json->{items}};
            my @items       = @{$more_json->{entry_data}->{ProfilePage}->[0]->{user}->{media}->{nodes}};
            my $item_count  = scalar(@items);
                
            if ($item_count == 0 ){
                   &w_error("Invalid Item Count 0 from the URL : ", $self->{profile}, $self->{profile_cat});
                   return [];
            }

            $max_id         = $items[$item_count - 1]->{id};

            # Set the availability for the next set of documents.
            $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{moreAvailable} = $more_json->{more_available};

            # Save all these posts in the global frontpage object
            push(@{$self->{frontpage}->{entry_data}->{UserProfile}->[0]->{userMedia}}, @items);
            push(@{$self->{cur_posts}}, @items);

            $self->{cur_level}++;
            return \@items;
        #}
    } else {
        &w_info("(deep crawl bail) Can deep crawl : ".($self->can_deep_crawl ? "yes" : "no").", current crawl level = ".$self->{cur_level}." out of ".$self->{max_deep_crawl_levels}, $self->{profile}, $self->{profile_cat});
    }
    return [];
}

1;

