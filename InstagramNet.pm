package InstagramNet;

use strict;
use warnings;
use utf8;
use InstagramLog;
use LWP::UserAgent;
use constant MY_USER_AGENT => 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.4)';

sub new {
    my ($class, $profile, $category, $host, $port, $options) = @_;
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    $ua->env_proxy;
    $ua->agent(MY_USER_AGENT);

    return bless {
        profile  => $profile,
        category => $category,
        ua       => $ua,
        host     => $host,
        port     => $port,
        base_uri => "http://$host/"
    }, $class;
}

sub ua { shift->{ua} }
sub base { shift->{base_uri} }

sub request {
    my ($self, $method, $uri, $content, $referer) = @_;
    my $full_uri = $self->base . $uri;
    my $req;

    &w_info("Fetching $full_uri", $self->{profile}, $self->{category});

    if (defined $content) {
        $req = HTTP::Request->new( $method, $full_uri, undef, $content );
        $req->header('Content-Type' => 'application/json');
        $req->header('Referer' => $referer) if defined $referer;
    } else {
        $req = HTTP::Request->new( $method, $full_uri );
    }

    my $response = $self->ua->request($req);

    if ($response->is_success) {
        return {
            status => $response->status_line,
            content => $response->content,
        };
    } else {
        return {
            status => $response->status_line,
            content => $response->content,
        };
    }
}

sub get {
    my ($self, $url, $referer) = @_;
    $self->request(GET => $url);
}

1;
