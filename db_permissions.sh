for HOST in `awk '/crawl/ {print $2}' hosts.txt`
do
	{
		echo "GRANT ALL PRIVILEGES ON instagram_crawl.* to instagram@$HOST identified by 'instagrampass';";
		echo "GRANT ALL PRIVILEGES ON instagram_universe.* to instagram@$HOST identified by 'instagrampass';";
		echo "FLUSH PRIVILEGES;";
	} | mysql -u root
done
