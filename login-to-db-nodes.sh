#!/bin/bash

for host in `awk '/db/ {print $3}' hosts.txt`
do
    echo "Logging in to $host"
    ssh -o 'StrictHostKeyChecking=no' root@$host "$@"
done
