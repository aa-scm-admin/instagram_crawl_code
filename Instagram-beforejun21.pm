package Instagram;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: HTTP Scraping Interface For http://stagram.com
# Date: May/2014

use utf8;
use strict;
use warnings;
use InstagramNet;
use InstagramLog;
use Data::Dumper;
use JSON;

sub new {
    my ($class, $config, $profile, $profile_cat, $deep_crawl, $deep_crawl_levels) = @_;
    return bless {
        config                => $config,
        profile               => $profile,
        profile_cat           => $profile_cat,
        can_deep_crawl        => $deep_crawl,
        max_deep_crawl_levels => defined $deep_crawl_levels ? int($deep_crawl_levels) : 2,
        csrf_token            => '',
        cur_level             => 0,
        last_code             => ''
    }, $class;
}

sub extract_json($) {
    my ($self, $data) = @_;
    my @lines = split(/\n/, $data);
    my $json;

    foreach (@lines) {
        chomp;
        if ($_ =~ /window._sharedData/) {
            $json = $_;
            last;
        }
    }

    if (defined $json) {
        &w_debug("Grabbed the primary JSON", $self->{profile}, $self->{profile_cat});
        $json =~ s/^.*window._sharedData\s*=\s*//;
        $json =~ s/};<\/script>/}/;
    } else {
        &w_error("FATAL: Unable to extract JSON from page. Investigate and check the page structure.", $self->{profile}, $self->{profile_cat});
    }

    return $json;
}

sub get_last_code() {
    my $self = shift;
    return $self->{last_code} || '';
}

sub get_frontpage_posts() {
    my $self     = shift;
    my $insta    = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);
    my $doc      = $insta->get($self->{profile}, "http://instagram.com/");
    $self->{last_code} = $doc->{status};
    # After a page is found it can be a 404 as well
    if ($doc->{status} ne '200 OK') {
        $self->{last_code} = $doc->{status};
        &w_error("Recieved ".$self->{last_code}." While crawling. Not crawling profile.", $self->{profile}, $self->{profile_cat});
        return [];
    }

    my $raw_json = $self->extract_json($doc->{content});

    eval {
        $self->{frontpage} = decode_json($raw_json);
    };
    if ($@) {
        &w_error("Error occured while processing frontpage. $!", $self->{profile}, $self->{profile_cat});
        return [];
    }

    $self->{csrf_token}    = $self->{frontpage}->{config}->{csrf_token};
    $self->{cur_posts}     = $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{userMedia};
    $self->{user_stats}    = $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{user};
    $self->{pages_crawled} = 1;

    return $self->{cur_posts};
}

sub get_csrf_token() {
    my $self = shift;
    return $self->{csrf_token};
}

#sub get_posts() {
#    my ($self) = @_;
#    return $self->{cur_posts};
#}

sub get_comments() {
    my ($self, $post) = @_;
    return $post->{comments}->{data};
}

sub get_likes() {
    my ($self, $post) = @_;
    return $post->{likes}->{data};
}

sub get_user_stats() {
    my ($self) = @_;
    return $self->{user_stats};
}

sub can_deep_crawl() {
    my ($self) = @_;
    return $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{moreInitiallyAvailable};
}

sub get_next_posts() {
    my ($self, $metrics) = @_;
    # 1. Find the max post-id
    # 2. Make a http-request. merge the posts
    if ($self->can_deep_crawl && ($self->{max_deep_crawl_levels} > 0 && $self->{cur_level} < $self->{max_deep_crawl_levels})) {
        $metrics->{num_http_reqs}++;
        my @maxels      = @{$self->{frontpage}->{entry_data}->{UserProfile}->[0]->{userMedia}};
        my $maxel_count = scalar(@maxels);
        my $max_id      = $maxels[$maxel_count - 1]->{id};
        my $insta       = InstagramNet->new($self->{profile}, $self->{profile_cat}, "instagram.com", 80);
        my $url         = $self->{profile}."/media?max_id=$max_id";
        my $more_raw    = $insta->get($url, "http://instagram.com/".$self->{profile});
        my $more_json   = { status => "notok"};
        if ($more_raw->{status} ne '200 OK') {
            &w_error("Got ".$more_raw->{status}." while downloading $url", $self->{profile}, $self->{profile_cat});
            return [];
        }
        eval {
            $more_json  = decode_json($more_raw->{content});
        };
        if ($@) {
            &w_error("Error occured while processing JSON response from $url : $@", $self->{profile}, $self->{profile_cat});
        }
        if ($more_json->{status} eq 'ok') {
            my @items       = @{$more_json->{items}};
            my $item_count  = scalar(@items);
                
            if ($item_count == 0 ){
                   &w_error("Invalid Item Count 0 from the URL : ", $self->{profile}, $self->{profile_cat});
                   return [];
            }

            $max_id         = $items[$item_count - 1]->{id};
            # Set the availability for the next set of documents.
            $self->{frontpage}->{entry_data}->{UserProfile}->[0]->{moreAvailable} = $more_json->{more_available};
            # Save all these posts in the global frontpage object
            push(@{$self->{frontpage}->{entry_data}->{UserProfile}->[0]->{userMedia}}, @items);
            $self->{cur_level}++;
            return \@items;
        }
    } else {
        &w_info("(deep crawl bail) Can deep crawl : ".($self->can_deep_crawl ? "yes" : "no").", current crawl level = ".$self->{cur_level}." out of ".$self->{max_deep_crawl_levels}, $self->{profile}, $self->{profile_cat});
    }
    return [];
}

1;
