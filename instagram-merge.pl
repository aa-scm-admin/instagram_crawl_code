#!/usr/bin/env perl -wl

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Main Merge program
# Date: Jun/2014

use strict;
use warnings;
use utf8;
use Data::Dumper;
use File::Slurp qw(read_file);
use JSON;
use Instagram;
use InstagramMergeDB;
use InstagramLog;
use Cwd;
use FindBin;
use File::Spec;
use Getopt::Long;

sub help {
    print "syntax: $0 [--daemon] [--debug] [--verbose] category merge_day<yyyymondd>";
    exit 0;
}

my ($opt_daemon, $opt_debug, $opt_verbose, $category,$mergeday );
GetOptions(
    "daemon|d" => \$opt_daemon,
    "debug|e"  => \$opt_debug,
    "verbose|v" => \$opt_verbose,
    "category=s"      => \$category,
    "mergeday=s"      => \$mergeday,

) || &help;

#my $category     = shift;
#my $mergeday = shift;
my $debug        = (defined $ENV{DEBUG} || $opt_debug ) ? 1 : 0;
my $verbose      = (defined $ENV{VERBOSE} || $opt_verbose ) ? 1 : 0;
my $cwd          = getcwd;
my $mypath       = $FindBin::Bin;
my $stopper_file = "$mypath/stopper";
my $merge_date = &convert_day_to_date($mergeday);

print "mergeday ",$mergeday ," category ", $category,"\n";

print "mergedate ",$merge_date, "\n"; 
#exit;


#function 
sub convert_day_to_date{
   my $mergeday = shift;
   my ( $year,$month,$day);
   #get the year 
   $year=substr($mergeday, 0, 4);
   #get the month
   $month=substr($mergeday, 4, 3);
   #convert to number
   my %month_hash;
   @month_hash{qw(jan feb mar apr may jun jul aug sep oct nov dec)} = 1..12;

   #get the day
   $day=substr($mergeday,7);

   #combine together 
   my $string = join('-', $year,$month_hash{$month},$day);
   #return
   return $string;
}



# Stopper file changes per category
if (defined $category) {
    $stopper_file= "$stopper_file.$category";
}

binmode(STDIN,  ':encoding(UTF-8)');
binmode(STDOUT, ':encoding(UTF-8)');
binmode(STDERR, ':encoding(UTF-8)');

my $config   = {};

sub read_config {
    my $path = shift;
    my $c;
    eval {
        my $d = read_file($path);
        $d    =~ s/\/\*.*?\*\///g;
        $c    = decode_json($d);
        if (not defined $c->{version}) {
            &w_exit(".version does not exist in $path Aborting");
        }
        $ENV{LOG_FORMAT}         = $c->{log_format};
    };
    if ($@) {
        print $@;
        exit 1;
    }
    return $c
}

$config = &read_config("$mypath/Instagram.json");

# Main program starts here
my $total_processed = 0;
my $total_relationships = 0;
my $total_new_users = 0;
my $summary = {
    total_relationships => 0,
    total_profiles_processed => 0,
    total_new_users => 0,
    relationships => {
        inserted_new => 0,
        updated_existing => 0,
        no_change => 0,
        unknown => 0
    }
};
if (-e $stopper_file) {
    # Exit early if stopper file is found.
    &w_info("Found stopper file $stopper_file. Exiting ....");
} else {
    print("Alrite ... let the fun of merging begin ...");
    # Continue normal processing
    my $mdb = InstagramMergeDB->new($config);
    $mdb->setup();
    my $tot_seq = $mdb->countProfilesToBeMerged($category);
    my $cur_seq  = 1;
    while (my @profile = $mdb->fetchProfileToMerge($category)) {
        &w_exit("No more profiles found.") if scalar(@profile) != 3;
	push @profile,$merge_date;
        my $ret_map = $mdb->mergeProfile(@profile);

        $mdb->markProfileMerged($profile[1], $ret_map, $profile[2], $tot_seq, $cur_seq);
        $summary->{total_profiles_processed}++;
        $summary->{total_relationships}               += $ret_map->[0];
        $summary->{total_new_users}                   += $ret_map->[1];
        $summary->{relationships}->{inserted_new}     += $ret_map->[2]->{inserted_new_fan_entity_map};
        $summary->{relationships}->{updated_existing} += $ret_map->[2]->{updated_entity_relbits};
        $summary->{relationships}->{no_change}        += $ret_map->[2]->{no_change};
        $summary->{relationships}->{unknown}          += $ret_map->[2]->{unknown};
        if (-e $stopper_file) {
            &w_info("Found stopper file $stopper_file. Exiting ....");
            last;
        }
        $cur_seq++;
    }
}

print '=' x 87;
print "\t\t\t\t\tSUMMARY";
print '=' x 87; print '';
print(sprintf "[summary] Processed %8d Profiles.", $summary->{total_profiles_processed}, "global");
print(sprintf "[summary] Processed %8d records from entity_fan table", $summary->{total_relationships}, "global");
print(sprintf "[summary] Added     %8d new users into UUID_ig table", $summary->{total_new_users}, "global");
print(sprintf "[summary] Inserted  %8d records (fan<->entity relationships) into entity_fan_XXX_master tables.", $summary->{relationships}->{inserted_new}, "global");
print(sprintf "[summary] Updated   %8d records (relationship bits or timestamp change) in entity_fan_XXX_master tables.", $summary->{relationships}->{updated_existing}, "global");
print(sprintf "[summary] No-Change %8d records from entity_fan haven't made any changes to entity_fan_XXX master tables.", $summary->{relationships}->{no_change}, "global");
print(sprintf "[summary] Unknown   %8d records", $summary->{relationships}->{unknown}, "global");
print '';
print 'All Done. Good Bye.';
print '=' x 87; print '';
exit(0);
