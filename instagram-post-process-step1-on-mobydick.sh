#!/usr/bin/bash

#Login to mysql DB source
for host in `awk '/db/ {print $3}' hosts.txt`
do
    echo "Logging in to $host"
    ssh -o 'StrictHostKeyChecking=no' root@$host "$@"
done

pwd

#Checking the instagram_crawl job_entities status
echo "select status,crawl_code,count(*) from job_entities group by status,crawl_code"
mysql -uroot instagram_crawl -e "select status,crawl_code,count(*) from job_entities group by status,crawl_code";
          if [ $? != 0 ]
          then
             echo "mysql job_entities process status has failed"
             exit
          else
             fi

#Checking the entity_fan count status
echo "select count(*) from entity_fan"
mysql -uroot instagram_crawl -e "select count(*) from entity_fan";
          if [ $? != 0 ]
          then
             echo "entity_fan count query execution failed"
             exit
          else
             fi

#Checking the 404 entities count list
echo "select aa_id as 'S.No', category, profile_id, url, crawl_code, crawl_hostname from job_entities where crawl_code like '%404%'"
mysql -uroot instagram_crawl -e "select aa_id as 'S.No', category, profile_id, url, crawl_code, crawl_hostname from job_entities where crawl_code like '%404%'";
          if [ $? != 0 ]
          then
             echo "404 Entities found list process has failed"
             exit
          else
             fi

#checking the 200OK entities count list
echo "select aa_id as 'S.No', category, profile_id, url, crawl_code from job_entities where status = 'crawled' and  num_http_reqs = 0 and num_posts = 0"
mysql -uroot instagram_crawl -e "select aa_id as 'S.No', category, profile_id, url, crawl_code from job_entities where status = 'crawled' and  num_http_reqs = 0 and num_posts = 0";
          if [ $? != 0 ]
          then
             echo "200OK Entities found list process has failed"
             exit
          else
             fi

#checking the 200OK entities count list
echo "SHOW VARIABLES LIKE 'secure_file_priv'"
mysql -uroot instagram_crawl -e "SHOW VARIABLES LIKE `secure_file_priv`";
          if [ $? != 0 ]
          then
             echo "secure_file_priv check process has failed"
             exit
          else
             fi

#echo "secure-file-priv = """ >> /etc/my.cnf
#sed -i '/thread_cache_size/asecure-file-priv = ""' /etc/my.cnf

#Restarting the Mysql services on the DB Machine
echo "Restart Mysql services"
#sudo service mysqld restart
          if [ $? != 0 ]
          then
             echo "Mysql Process has not restarted"
             exit
             echo "Mysql services has restarted"
          else
             fi

#checking the 200OK entities count list
echo "SHOW VARIABLES LIKE 'secure_file_priv'"
mysql -uroot instagram_crawl -e "SHOW VARIABLES LIKE `secure_file_priv`";
          if [ $? != 0 ]
          then
             echo "secure_file_priv check process has failed"
             exit
          else
             fi

#Checking the instagram_crawl job_entities status
echo "select status,crawl_code,count(*) from job_entities group by status,crawl_code"
mysql -uroot instagram_crawl -e "select status,crawl_code,count(*) from job_entities group by status,crawl_code";
          if [ $? != 0 ]
          then
             echo "mysql job_entities process status has failed"
             exit
          else
             fi

#Checking the entity_fan count status
echo "select count(*) from entity_fan"
mysql -uroot instagram_crawl -e "select count(*) from entity_fan";
          if [ $? != 0 ]
          then
             echo "entity_fan count query execution failed"
             exit
          else
             fi


#talking the database from the ec2 server
echo "sh backup-database-from-ec2-server.sh `date`"
#sh backup-database-from-ec2-server.sh
if [ $? != 0 ]
          then
             echo "Backup-Database from ec2-server process has failed"
             exit
          else
             echo "Backup-Databse from ece2-server has done successfully `date`"
             fi

