package InstagramBitmapVerify;

# Author: Naresh <nareshv@affinityanswers.com>
# Description: Validate the bits associated with the processed fans from entity_fan with master tables
# Date: Jun/2014

use strict;
use warnings;
use utf8;
use DBI;
use Data::Dumper;
use InstagramLog;
use Sys::Hostname;

my $debug    = defined $ENV{DEBUG} ? 1 : 0;
my $verbose  = defined $ENV{VERBOSE} ? 1 : 0;
my $show_sql = defined $ENV{SHOW_SQL} ? 1 : 0;

sub new {
    my $class = shift;
    my $config= shift;
    my $self  = {
        _dbh        => undef,
        _myname     => hostname,
        rels        => {},
        config      => $config
    };
    print Dumper($self) if $debug;
    $self->{_myname} =~ s/\.(colligent|affinityanswers)\.com//;
    bless($self, $class);    # make $self an object of class $class
    return $self;
}

sub setup() {
    my $self = shift;
    my $cfg  = $self->{config}->{db}->{merge};
    $self->{_dbh} = DBI->connect(
        'DBI:mysql:database='.$cfg->{db}.';host='.$cfg->{host}, $cfg->{user}, $cfg->{pass}
    ) || &w_exit("Error: Could not connect to database: $DBI::errstr");

    # Set the sql strict mode
    $self->{_dbh}->do("set sql_mode='STRICT_ALL_TABLES'") || &w_exit("Error: Failed to set strict mode.");
}

sub getTableName($) {
    my $self  = shift;
    my $name  = shift;
    my $table = $self->{config}->{tables}->{$name};
    if (not defined $table) {
        &w_exit("'$name' is not found in Instagram.json. Aborting");
    }
    return $table;
}

sub relationshipBits {
    my ($self, $rel) = @_;
    if (defined $rel) {
        return $self->rel_fw($rel);
    }
    die("Invalid $rel. Aborting");
}

sub validateBitsForProfile {
    my ($self, $profile, $cat, $profile_uid) = @_;
    my $tbl  = $self->getTableName('entity_fan');
    my $tbl2 = $self->getTableName('relationship_map');
    my $sql  = "select count(distinct userid) as count, fan_relationship, id from $tbl A right join $tbl2 B on A.fan_relationship = B.rel where profile_id = ? group by fan_relationship";
    my $sth = $self->{_dbh}->prepare($sql);
    $sth->execute($profile) || die $!;
    while(my $row = $sth->fetchrow_hashref) {
        my $bit_val = $row->{id};
        my $rel     = $row->{fan_relationship};
        my $fanc    = $row->{count};
        my $vql = "select count(*) as fans, fan_relationship & $bit_val as relationship from instagram.entity_fan_${cat}_master where entity_uid = ? and fan_relationship & $bit_val > 0 group by relationship";
        my $vth = $self->{_dbh}->prepare($vql);
        $vth->execute($profile_uid) || die $!;
        my $ret = $vth->fetchrow_hashref;
        if ($fanc > $ret->{fans}) {
            printf STDOUT "BAD: %20s : relationship = %28s/%d || Fans (entity_fan, entity_fan_${cat}_master) = (%d, %d)\n", $profile, $rel, $bit_val, $fanc, $ret->{fans};
        } else {
            printf STDOUT " OK: %20s : relationship = %28s/%d || Fans (entity_fan, entity_fan_${cat}_master) = (%d, %d)\n", $profile, $rel, $bit_val, $fanc, $ret->{fans};
        }
    }
}

sub fetchProfileToValidate {
    my ($self, $category) = @_;
    my $tbl  = $self->getTableName('job_entities');
    my $sql  = "select * from $tbl where status = 'merged'";
    if (defined $category and length($category) == 3) {
        $sql  = "select * from $tbl where status = 'merged' and category = '$category'";
    }
    my $sth  = $self->{_dbh}->prepare($sql);
    $sth->execute() || die $!;
    my $profile = $sth->fetchrow_hashref;
    if (defined $profile) {
        $self->{_dbh}->do("update $tbl set status = 'validated' where profile_id = '".$profile->{profile_id}."'") || die $!;
    }
    return $profile;
}

sub processProfile {
    my ($self, $profile) = @_;
    return $self->validateBitsForProfile(
        $profile->{profile_id},
        $profile->{category},
        $profile->{aa_id}
    );
}

sub resetAll {
    my $self = shift;
    my $tbl  = $self->getTableName('job_entities');
    return $self->{_dbh}->do("update $tbl set status = 'merged' where status = 'validated'");
}
1;
