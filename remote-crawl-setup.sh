#!/bin/bash

if [ -z $1 ];then
    echo "syntax: $0 <2014aug27>"
    exit
fi

catalog=$1

grep ^crawl hosts.txt | while read line; do echo $line | NODE_ENV=new-crawl-node ./setup-ec2.sh $catalog $(awk '/crawl/ {print $3, $2}') centos; done

