#!/bin/bash


UUID_ig=all_networks.UUID_ig
entity_fan=instagram_crawl.entity_fan
entity_fan_ALL_master=instagram.entity_fan_ALL_master
PRIVS="-uinstagram -pinstagrampass -A -hstage5 instagram_crawl"
echo "Started one liner merge process...."
{
    # 1. Insert all the new fans into UUID_ig table
    echo "INSERT IGNORE INTO $UUID_ig (fan_id, last_updt_time) (SELECT userid, from_unixtime(IF(created_time <= 0, unix_timestamp(fetch_time), created_time)));"
    # 2. Update the entity_fan table with the Autoincremented values from UUID_ig (or JOIN ?)
    echo "ALTER IGNORE TABLE $entity_fan ADD fan_uuid bigint(20) unsigned;"
    echo "UPDATE $entity_fan A JOIN $UUID_ig B ON A.fan_id = B.fan_id set A.fan_uuid = B.uuid;"
    # 3. Update all the master merge tables as per category of the profile
    for CAT in ART BRN
    do
        echo "insert into instagram.entity_fan_${CAT}_master (entity_uid, fan_id, uuid, fan_relationship, last_act_time) (select entity_uid, fan_id, uuid, BIT_OR(fan_relationship) fan_relationship, from_unixtime(min(IF(created_time <= 0, unix_timestamp(fetch_time), created_time))) max_time from $entity_fan group by entity_uid, uuid) on duplicate key update fan_relationship = fan_relationship | VALUES(fan_relationship), last_act_time = GREATEST(last_act_time, VALUES(last_act_time));"
    done
    # 4. Update the UUID_ig table with the max activity time of the profile from entity_fan_ALL_master (merge) table.
    echo "UPDATE $UUID_ig A JOIN $entity_fan_ALL_master B ON A.uuid = B.uuid set A.fan_uuid = MAX(B.uuid);"
}
#} | mysql $PRIVS
